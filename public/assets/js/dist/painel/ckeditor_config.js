/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */
/*asfafaafa*/
CKEDITOR.editorConfig = function( config ) {

    config.extraPlugins = 'injectimage,oembed,font,panelbutton,colorbutton';

    config.toolbarGroups = [];
    config.toolbar = [
        [ 'Bold', 'Italic', 'Underline', 'Subscript', 'Superscript' ],
        [ 'NumberedList', 'BulletedList' ],
        [ 'Link', 'Unlink' ],
        [ 'oembed', 'InjectImage' , 'SpecialChar', 'Source'],
        [ 'FontSize', 'TextColor']
    ];

    config.allowedContent = true;
    config.removeButtons = '';

    config.format_tags = 'p;h1;h2;h3;pre';

    config.removeDialogTabs = 'image:advanced;link:advanced';
};
