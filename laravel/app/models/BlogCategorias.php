<?php

class BlogCategorias extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_categorias';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    /* SCOPES */

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
        return $query->where('id', '!=', $id);
    }

    /* RELATIONSHIPS */
    public function posts()
    {
    	return $this->hasMany('BlogPosts', 'blog_categorias_id');
    }
}