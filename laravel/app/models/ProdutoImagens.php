<?php

class ProdutoImagens extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos_imagens';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    /* SCOPES */

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc');
    }

    /* RELATIONSHIPS */
    public function produto()
    {
    	return $this->belongsTo('Produtos', 'produtos_id');
    }

}