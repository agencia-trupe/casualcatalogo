<?php

class ProgramaLojas extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'programa_lojas';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('titulo', 'asc');
    }

    public function assistentes(){
    	return $this->hasMany('User', 'programa_lojas_id');
    }

    public function participantes(){
    	return $this->hasMany('UsuariosCatalogo', 'programa_lojas_id');
    }
}