<?php

class Produtos extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    /* SCOPES */
    public function scopeAleatorio($query)
    {
        return $query->orderBy(\DB::raw('RAND()'));
    }

	public function scopeOrdenado($query)
    {
    	return $query->orderBy('ordem', 'asc');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeNotThis($query, $id)
    {
        return $query->where('id', '!=', $id);
    }

    public function scopeListaveis($query)
    {
        return $query->where('mostrar_em_listas', '=', 1);
    }

    public function scopeSite($query)
    {
        return $query->whereHas('fornecedor' , function($q){
            $q->where('publicar_site', '=', 1);
        })->where('publicar_site', '=', 1);
    }

    public function scopeCatalogo($query)
    {
        //return $query->where('publicar_catalogo', '=', 1);
        return $query->whereHas('fornecedor' , function($q){
            $q->where('publicar_catalogo', '=', 1);
        })->where('publicar_catalogo', '=', 1);
    }

    public function scopeDivisao($query, $divisao)
    {
        return $query->where('divisao', '=', $divisao);
    }

    public function scopeOrigem($query, $origem)
    {
        return $query->where('origem', '=', $origem);
    }

    public function scopeBuscaPorCodigo($query, $codigo)
    {
        return $query->where('codigo', 'like', '%'.$codigo.'%');
    }

    /* RELATIONSHIPS */
    public function fornecedor()
    {
    	return $this->belongsTo('Fornecedores', 'fornecedores_id');
    }

    public function tipo()
    {
        return $this->belongsTo('Tipos', 'tipos_id');
    }

    public function linha()
    {
        return $this->belongsTo('Linhas', 'linhas_id');
    }

    public function imagens()
    {
        return $this->hasMany('ProdutoImagens', 'produtos_id');
    }

    public function arquivos()
    {
        return $this->hasMany('ProdutosArquivos', 'produtos_id');
    }

    public function materiais()
    {
        return $this->belongsToMany('ProdutosMateriais', 'produtos_has_produtos_materiais', 'produtos_id', 'produtos_materiais_id');
    }

    public function tags()
    {
        return $this->belongsToMany('ProdutosTags', 'produtos_has_produtos_tags', 'produtos_id', 'produtos_tags_id');
    }

    public function cores()
    {
        return $this->belongsToMany('ProdutosCores', 'produtos_has_produtos_cores', 'produtos_id', 'produtos_cores_id');
    }

    public function acessos()
    {
        return $this->hasMany('ProdutosAcessos', 'produtos_id')->orderBy('produtos_acessos.updated_at', 'desc');
    }

}