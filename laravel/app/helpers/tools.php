<?php
class Tools {

    public static function diasFaltantes()
    {
        $campanha = ProgramaCampanhas::ativa()->first();

        if(is_null($campanha)) return 'X';

        $hoje = Date('Y-m-d');
        $data_termino = $campanha->data_termino;

        return Tools::diferenca($data_termino, $hoje);
    }

    private static function diferenca($data1, $data2){
        return (strtotime($data1) - strtotime($data2)) / (60 * 60 * 24);
    }

    public static function caminho_absoluto_newsletter($texto = ''){
        return str_replace("\"/assets/", "\"http://www.casualmoveis.com.br/assets/", $texto);
    }

    public static function debugArray($arr, $morrer = true)
    {
        echo '<pre>';
        is_array($arr) ? print_r($arr) : var_dump($arr);
        echo '</pre>';
        if($morrer)
            die();
    }

    public static function exclusaoElementosFilhos($model, $object)
    {
        $resposta_template = "data-filhos='###'";
        $mensagem = array();

        if($model == 'blog_categorias')
        {
            if(count($object->posts)){
                $mensagem["posts"] = count($object->posts);
                $contador_comentarios = 0;
                foreach ($object->posts as $post) {
                    if(count($post->comentarios)){
                        $contador_comentarios += count($post->comentarios);
                    }
                }
                if($contador_comentarios > 0) $mensagem["comentários"] = count($contador_comentarios);
            }
        }

        if($model == 'blog_posts')
        {
            if(count($object->comentarios))
                $mensagem["comentários"] = count($object->comentarios);
        }

        if($model == 'fornecedores')
        {
            if(count($object->arquivos))
                $mensagem["arquivos"] = count($object->arquivos);
            if(count($object->linhas))
                $mensagem["linhas"] = count($object->linhas);
            if(count($object->produtos))
                $mensagem["produtos"] = count($object->produtos);
        }

        if($model == 'linhas')
        {
            if(count($object->produtos))
                $mensagem["produtos"] = count($object->produtos);
        }

        if($model == 'tipos')
        {
            if(count($object->produtos))
                $mensagem["produtos"] = count($object->produtos);
        }

        if($model == 'produtos')
        {
            if(count($object->imagens))
                $mensagem["imagens"] = count($object->imagens);
            if(count($object->arquivos))
                $mensagem["arquivos"] = count($object->arquivos);
        }

        return empty($mensagem) ? '' : str_replace('###', json_encode($mensagem), $resposta_template);
    }

    public static function converteData($data = '')
    {
        if($data != ''){
        	if(strpos($data, '-') !== FALSE){
        		// Formato Americano -> Converter para BR
        		list($ano, $mes, $dia) = explode('-', $data);
        		return $dia.'/'.$mes.'/'.$ano;
        	}elseif(strpos($data, '/') !== FALSE){
        		// Formato BR -> Converter para Americano
        		list($dia, $mes, $ano) = explode('/', $data);
        		return $ano.'-'.$mes.'-'.$dia;
        	}
        }else{
        	return '';
        }
    }

    public static function exibeData($data = '')
    {

        $meses = array(
            '01' => 'jan',
            '02' => 'fev',
            '03' => 'mar',
            '04' => 'abr',
            '05' => 'mai',
            '06' => 'jun',
            '07' => 'jul',
            '08' => 'ago',
            '09' => 'set',
            '10' => 'out',
            '11' => 'nov',
            '12' => 'dez'
        );

        if ($data != '') {
            if(strpos($data, '-') !== FALSE){
                // Formato Americano -> Converter para BR
                list($ano, $mes, $dia) = explode('-', $data);
                return $dia.' '.strtoupper($meses[$mes]).' '.$ano;
            }elseif(strpos($data, '/') !== FALSE){
                // Formato BR -> Converter para Americano
                list($dia, $mes, $ano) = explode('/', $data);
                return $dia.' '.strtoupper($meses[$mes]).' '.$ano;
            }
        } else {
            return '';
        }

    }

    public static function mesExtenso($mes){
        if(strlen($mes) == 1) $mes = '0'.$mes;

        $meses = array(
            '01' => 'janeiro',
            '02' => 'fevereiro',
            '03' => 'março',
            '04' => 'abril',
            '05' => 'maio',
            '06' => 'junho',
            '07' => 'julho',
            '08' => 'agosto',
            '09' => 'setembro',
            '10' => 'outubro',
            '11' => 'novembro',
            '12' => 'dezembro'
        );

        if ($mes != '') {
            return $meses[$mes];
        } else {
            return '';
        }
    }

    public static function validarData($data)
    {
        if($data != ''){
            if(strpos($data, '-') !== FALSE){
                // Formato Americano
                list($ano, $mes, $dia) = explode('-', $data);
                $ano_ok = (int) $ano > 0 && (int) $ano < 3000;
                $mes_ok = (int) $mes > 0 && (int) $mes <= 12;
                $dia_ok = (int) $dia > 0 && (int) $dia <= 31;
                return $ano_ok && $mes_ok && $dia_ok;
            }elseif(strpos($data, '/') !== FALSE){
                // Formato BR
                list($dia, $mes, $ano) = explode('/', $data);
                $ano_ok = (int) $ano > 0 && (int) $ano < 3000;
                $mes_ok = (int) $mes > 0 && (int) $mes <= 12;
                $dia_ok = (int) $dia > 0 && (int) $dia <= 31;
                return $ano_ok && $mes_ok && $dia_ok;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static function ip()
    {
        if(isset($_SERVER["REMOTE_ADDR"]))
            return $_SERVER["REMOTE_ADDR"];
        elseif(isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        elseif(isset($_SERVER["HTTP_CLIENT_IP"]))
            return $_SERVER["HTTP_CLIENT_IP"];
    }

    public static function validarCpf($cpf)
    {
        $cpf = str_pad(preg_replace('/[^0-9]/i', '', $cpf), 11, '0', STR_PAD_LEFT);

        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'){
            return false;
        }
        else{
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }

    public static function prep_url($link)
    {
        if ($link == 'http://' OR $link == '') return '';

        $url = parse_url($link);

        if (!$url OR !isset($url['scheme']))
            $link = 'http://'.$link;

        return $link;
    }

    public static function embed($str = '', $width = FALSE, $height = FALSE, $removerLink = TRUE){

        //$str = stripslashes(htmlspecialchars_decode($str));

        if($width)
            $str = preg_replace('~width="(\d+)"~', 'width="'.$width.'"', $str);
        if($height)
            $str = preg_replace('~height="(\d+)"~', 'height="'.$height.'"', $str);

		if($removerLink) // SEM o link 'ver mapa ampliado'
            return preg_replace('~<br \/>(.*)~', '', $str);
        else // COM o link 'ver mapa ampliado'
            return $str;
    }
}
