<?php

class Usuarios {

	public static function isAdmin() {
		return \Auth::painel()->user()->admin_geral == 1;
	}

	public static function isAssistProdutos() {
		return \Auth::painel()->user()->assist_produtos == 1;
	}

	public static function isAssistPrograma() {
		return \Auth::painel()->user()->assist_programa == 1;
	}

	public static function isGerente() {
		return \Auth::painel()->user()->is_gerente == 1;
	}

	public static function isFuncionario() {
		return \Auth::catalogo()->user()->is_funcionario == 1;
	}
}