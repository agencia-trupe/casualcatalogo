<?php

namespace Site;

use \Controller, \Input, \Redirect, \Session, \View, \Request, \BlogPosts, \BlogTags, \BlogComentarios, \BlogCategorias;

class BlogController extends BaseController {

	protected $layout = 'frontend.templates.site';

	/* Página Inicial e Listagem por Categoria */
	public function index($slug_categoria = '')
	{	
		if($slug_categoria){
			$categoriaSelecionada = BlogCategorias::slug($slug_categoria)->first();

			if(!$categoriaSelecionada) $categoriaSelecionada = false;
		}else
			$categoriaSelecionada = false;

		/* CATEGORIAS COM POST */
		$listaCategorias = BlogCategorias::whereHas('posts', function($q){
												$q->publicados();
											})->get();

		/* ANOS COM POST */
		$listaAnos = BlogPosts::publicados()
								->distinct()
								->select(\DB::raw('YEAR(data) as ano'))
								->groupBy('ano')
								->orderBy('ano', 'DESC')
								->get();

		$listaTags = BlogTags::whereHas('posts', function($q){
											$q->publicados();
										})->ordenado()->get();

		if($slug_categoria){
			$listaPosts = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->filtrarCategoria($categoriaSelecionada->id)
																			 		  ->publicados()
																			 		  ->ordenado()
																			 		  ->paginate(3);
			
			$totalRegistros = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->filtrarCategoria($categoriaSelecionada->id)
																			 	 		  ->publicados()
																			 	 		  ->ordenado()	
																			 	 		  ->get();

			$filtro = '?origem=cat&slug='.$categoriaSelecionada->slug;
		}else{
			$listaPosts = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																			 		  ->ordenado()
																			 		  ->paginate(3);

			$totalRegistros = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																			 	 		  ->ordenado()
																			 	 		  ->get();
			$filtro = '';
		}


		$view = View::make('frontend.site.blog.index')->with(compact('listaCategorias'))
					   								  ->with(compact('listaAnos'))
													  ->with(compact('listaTags'))
													  ->with(compact('listaPosts'))
													  ->with(compact('categoriaSelecionada'))
													  ->with(compact('totalRegistros'))
													  ->with(compact('filtro'));

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;		

	}

	/* Listagem por (só ano) e por (ano + mês) */
	public function porAno($anoSelecionado = '', $mesSelecionado = '')
	{
		if(!$anoSelecionado) return Redirect::to('blog');

		/* CATEGORIAS COM POST */
		$listaCategorias = BlogCategorias::whereHas('posts', function($q){
												$q->publicados();
											})->get();

		/* ANOS COM POST */
		$listaAnos = BlogPosts::publicados()
								->distinct()
								->select(\DB::raw('YEAR(data) as ano'))
								->groupBy('ano')
								->orderBy('ano', 'DESC')
								->get();

		$meses = BlogPosts::publicados()
								->distinct()
								->select(\DB::raw('MONTH(data) as mes'))
								->groupBy('mes')
								->orderBy('mes', 'DESC')
								->whereRaw("YEAR(data) = $anoSelecionado")
								->get();

		$listaTags = BlogTags::whereHas('posts', function($q){
											$q->publicados();
										})->ordenado()->get();


		if(!$mesSelecionado){
			$listaPosts = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																			 		  ->ordenado()
																			 		  ->whereRaw("YEAR(data) = $anoSelecionado")
																			 		  ->paginate(3);

			$totalRegistros = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																			 			  ->ordenado()
																			 			  ->whereRaw("YEAR(data) = $anoSelecionado")
																			 			  ->get();
		}else{
			$listaPosts = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																			 		  ->ordenado()
																			 		  ->whereRaw("YEAR(data) = $anoSelecionado AND MONTH(data) = $mesSelecionado")
																			 		  ->paginate(3);

			$totalRegistros = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																			 			  ->ordenado()
																			 			  ->whereRaw("YEAR(data) = $anoSelecionado AND MONTH(data) = $mesSelecionado")
																			 			  ->get();
		}

		$filtro = '';

		$view = View::make('frontend.site.blog.index')->with(compact('listaCategorias'))
													  ->with(compact('listaAnos'))
													  ->with(compact('listaTags'))
													  ->with(compact('listaPosts'))
		    										  ->with(compact('anoSelecionado'))
													  ->with(compact('mesSelecionado'))
													  ->with(compact('meses'))
													  ->with(compact('totalRegistros'))
													  ->with(compact('filtro'));		

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;	
	}

	/* Listagem por tags */
	public function porTags($slug_tag = '')
	{
		if($slug_tag){
			$tagSelecionada = BlogTags::slug($slug_tag)->first();

			if(!$tagSelecionada) return Redirect::to('blog');
		}else
			return Redirect::to('blog');

		/* CATEGORIAS COM POST */
		$listaCategorias = BlogCategorias::whereHas('posts', function($q){
												$q->publicados();
											})->get();

		/* ANOS COM POST */
		$listaAnos = BlogPosts::publicados()
								->distinct()
								->select(\DB::raw('YEAR(data) as ano'))
								->groupBy('ano')
								->orderBy('ano', 'DESC')
								->get();

		$listaTags = BlogTags::whereHas('posts', function($q){
											$q->publicados();
										})->ordenado()->get();

	
		$listaPosts = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																				 ->ordenado()
																				 ->whereHas('tags', function($q) use ($tagSelecionada){
																				 	$q->slug($tagSelecionada->slug);
																				 })
																				 ->paginate(3);
		
		$totalRegistros = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																					 ->ordenado()
																					 ->whereHas('tags', function($q) use ($tagSelecionada){
																					 	$q->slug($tagSelecionada->slug);
																					 })
																					 ->get();																		 

		$filtro = '?origem=tag&slug='.$tagSelecionada->slug;
		$view = View::make('frontend.site.blog.index')->with(compact('listaCategorias'))
				   								     ->with(compact('listaAnos'))
													 ->with(compact('listaTags'))
													 ->with(compact('listaPosts'))
													 ->with(compact('tagSelecionada'))
													 ->with(compact('totalRegistros'))
													 ->with(compact('filtro'));		

		if(Request::ajax()){
			$sections = $view->renderSections();
	        return $sections['conteudo'];
		}
		else
			$this->layout->content = $view;	
	}

	public function detalhes($slug_post = '')
	{
		$filtro_origem = Input::has('origem') ? Input::get('origem') : '';
		$filtro_slug = Input::has('slug') ? Input::get('slug') : '';

		$categoriaSelecionada = null;
		$tagSelecionada = null;

		if(!$slug_post) return Redirect::to('blog');
		
		/* CATEGORIAS COM POST */
		$listaCategorias = BlogCategorias::whereHas('posts', function($q){
												$q->publicados();
											})->get();

		/* ANOS COM POST */
		$listaAnos = BlogPosts::publicados()
								->distinct()
								->select(\DB::raw('YEAR(data) as ano'))
								->groupBy('ano')
								->orderBy('ano', 'DESC')
								->get();

		$listaTags = BlogTags::whereHas('posts', function($q){
											$q->publicados();
										})->ordenado()->get();

		$detalhes = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																			    ->slug($slug_post)
																			    ->first();

		if(!$detalhes) App::abort('404');

		
		$filtro = '';

		// Veio da listagem de Categorias
		if($filtro_origem == 'cat')
		{
			$categoriaSelecionada = BlogCategorias::slug($filtro_slug)->first();

			$anterior = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																					->filtrarCategoria($categoriaSelecionada->id)
																					->invertido()
																					->where('data', '>', $detalhes->data)
																					->notThis($detalhes->id)
																			    	->first();

			$proximo  = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																					->filtrarCategoria($categoriaSelecionada->id)
																					->ordenado()
																					->where('data', '<', $detalhes->data)
																					->notThis($detalhes->id)
																			    	->first();

			$filtro = '?origem=cat&slug='.$categoriaSelecionada->slug;
		}
		// Veio da listagem de Tags
		elseif($filtro_origem == 'tag')
		{
			$tagSelecionada = BlogTags::slug($filtro_slug)->first();

			$anterior = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																					->whereHas('tags', function($q) use ($tagSelecionada){
																					 	$q->slug($tagSelecionada->slug);
																					 })
																					->invertido()
																					->where('data', '>', $detalhes->data)
																					->notThis($detalhes->id)
																			    	->first();

			$proximo  = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																					->whereHas('tags', function($q) use ($tagSelecionada){
																					 	$q->slug($tagSelecionada->slug);
																					 })
																					->ordenado()
																					->where('data', '<', $detalhes->data)
																					->notThis($detalhes->id)
																			    	->first();
			$filtro = '?origem=tag&slug='.$tagSelecionada->slug;																			    	
		}
		// Veio de qualquer outro lugar
		else
		{
			$anterior = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																					->invertido()
																					->where('data', '>', $detalhes->data)
																					->notThis($detalhes->id)
																			    	->first();

			$proximo  = BlogPosts::with('categoria', 'comentariosAprovados', 'tags')->publicados()
																					->ordenado()
																					->where('data', '<', $detalhes->data)
																					->notThis($detalhes->id)
																			    	->first();			
		}		

		$this->layout->content = View::make('frontend.site.blog.detalhes')->with(compact('listaCategorias'))
					   													  ->with(compact('listaAnos'))
																		  ->with(compact('listaTags'))
																		  ->with(compact('listaPosts'))
																		  ->with(compact('detalhes'))
																		  ->with(compact('categoriaSelecionada'))
																		  ->with(compact('tagSelecionada'))
																		  ->with(compact('proximo'))
																		  ->with(compact('anterior'))
																		  ->with(compact('filtro'));

	}

	public function comentar()
	{
		$id = Input::get('bpi');

		if(!$id) return Redirect::back();

		$blogpost = BlogPosts::find($id);

		if(!$blogpost) return Redirect::back();

		$cmt = new BlogComentarios;
		$cmt->blog_posts_id = $id;
		$cmt->autor = strip_tags(Input::get('comentario_nome'));
		$cmt->email = strip_tags(Input::get('comentario_email'));
		$cmt->texto = strip_tags(Input::get('comentario_msg'));
		$cmt->data = Date('Y-m-d');
		$cmt->aprovado = 0;
		$cmt->save();

		Session::flash('comentarioEnviado', true);
		return Redirect::back();
	}
}
