<?php

namespace Catalogo;

use \View, \Cart, \Redirect, \PedidosLojas, \Pedidos, \Auth, \Input, \PedidosItens, \Session, \Mail, \Config, \Log;

class PedidoController extends BaseController{

	protected $layout = 'frontend.templates.catalogo';

	public function checkout()
	{
		if(Cart::count(false) == 0) Return Redirect::to('catalogo');

		$listaLojas = PedidosLojas::ordenado()->get();

		$this->layout->content = View::make('frontend.catalogo.pedido.index')->with(compact('listaLojas'));
	}

	public function finalizar()
	{
		if(Cart::count(false) == 0) Return Redirect::to('catalogo');

		// Gravar Pedido no Banco
		$pedido = new Pedidos;
		$pedido->usuarios_catalogo_id = Auth::catalogo()->user()->id;
		$pedido->pedidos_lojas_id = Input::get('loja_destino');

		// MARCA COMO OUTLET (temporário)
		$pedido->outlet = true;

		$pedido->save();

		// Gravar Itens do pedido no Banco
		foreach (Cart::content() as $itemCarrinho){
			$item = new PedidosItens;
			$item->pedidos_id = $pedido->id;
			$item->produtos_id = $itemCarrinho->id;
			$item->quantidade = $itemCarrinho->qty;
			$item->observacoes = $itemCarrinho->options->observacoes;
			$item->save();
		}

		$data['loja'] = PedidosLojas::find(Input::get('loja_destino'));
		$data['carrinho'] = Cart::content();

		$data['solicitante'] = \Auth::catalogo()->user();

		if($data['solicitante']->telefone == '' && Input::has('usuario_catalogo_telefone')){
			$data['solicitante']->telefone = Input::get('usuario_catalogo_telefone');
			$data['solicitante']->save();
		}

		// Enviar por e-mail para a loja selecionada
		Mail::send('emails.orcamento', $data, function($message) use ($data)
		{
			$destino = isset($data['loja']->email) ? $data['loja']->email : 'orcamento@casualmoveis.com.br';

		    $message->to($destino, 'Casual')
		    		->subject('Pedido de Orçamento')
		    		->bcc('marcelo.cruz@casualmoveis.com.br', 'Marcelo Cruz')
					->bcc('gisa.morato@casualmoveis.com.br', 'Adalgisa Morato')
					->replyTo(Auth::catalogo()->user()->email, Auth::catalogo()->user()->nome);

		    if(Config::get('mail.pretend')) Log::info(View::make('emails.orcamento', $data)->render());
		});


		// Esvaziar o carrinho
		Cart::destroy();

		// Redirecionar para home do catálogo com mensagem de enviado
		Session::flash('cart-ended', 'Seu Pedido de Orçamento foi enviado com sucesso para a Loja selecionada!');
		return Redirect::to('catalogo');
	}

	public function pedidos()
	{
		$this->layout->content = View::make('frontend.catalogo.pedido.lista');
	}

	public function historico($id_pedido)
	{
		$detalhe = Pedidos::find($id_pedido);

		if(!$detalhe || $detalhe->usuarios_catalogo_id != Auth::catalogo()->user()->id) return Redirect::to('catalogo');

		$this->layout->content = View::make('frontend.catalogo.pedido.historico')->with(compact('detalhe'));
	}
}
