<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ProgramaCampanhas, \ProgramaEmails, \Validator;

class ProgramaEmailsController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$campanha_id = Input::get('campanha_id');
		if(!$campanha_id) return Redirect::to('painel/programacampanhas');

		$campanha = ProgramaCampanhas::find($campanha_id);
		if(!$campanha) return Redirect::to('painel/programacampanhas');

		$tipo = Input::has('tipo') ? Input::get('tipo') : 'arquiteto';

		$this->layout->content = View::make('backend.programaemails.index')->with(compact('campanha', 'tipo'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$campanha_id = Input::get('campanha_id');
		if(!$campanha_id) return Redirect::to('painel/programacampanhas');

		$campanha = ProgramaCampanhas::find($campanha_id);
		if(!$campanha) return Redirect::to('painel/programacampanhas');

		$tipo = Input::has('tipo') ? Input::get('tipo') : 'arquiteto';

		$this->layout->content = View::make('backend.programaemails.form')->with(compact('campanha', 'tipo'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(
			Input::all(),
			array(
			    'inicio_pontuacao' => 'required|menor_que:'.Input::get('fim_pontuacao').'|intervalo_pontos_ja_existe_emails:'.Input::get('programa_campanha_id').','.Input::get('tipo').'|contem_intervalo_pontos_emails:'.Input::get('programa_campanha_id').','.Input::get('fim_pontuacao').','.Input::get('tipo'),
				'fim_pontuacao' => 'required|maior_que:'.Input::get('inicio_pontuacao').'|intervalo_pontos_ja_existe_emails:'.Input::get('programa_campanha_id').','.Input::get('tipo').'|contem_intervalo_pontos_emails:'.Input::get('programa_campanha_id').','.Input::get('inicio_pontuacao').','.Input::get('tipo'),
				'programa_campanha_id' => 'required|numeric'
			),
			array(
			    'menor_que' => 'O início da faixa de Pontuação deve ser menor que o término',
			    'maior_que' => 'O término da da faixa de Pontuação deve ser maior que o início',
			    'intervalo_pontos_ja_existe_emails' => 'Já existe um email para o intervalo selecionado nessa Campanha',
				'contem_intervalo_pontos_emails' => 'Já existe um email dentro do intervalo selecionado nessa Campanha',
			)
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$object = new ProgramaEmails(Input::all());
		$object->tipo = 'arquiteto';

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Email criado com sucesso.');
			return Redirect::route('painel.programaemails.index', array('campanha_id' => $object->programa_campanha_id, 'tipo' => $object->tipo));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Email!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$campanha_id = Input::get('campanha_id');
		if(!$campanha_id) return Redirect::to('painel/programacampanhas');

		$campanha = ProgramaCampanhas::find($campanha_id);
		if(!$campanha) return Redirect::to('painel/programacampanhas');

		$this->layout->content = View::make('backend.programaemails.edit')->with('registro', ProgramaEmails::find($id))
																		  ->with('campanha', $campanha);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validator = Validator::make(
			Input::all(),
			array(
			    'inicio_pontuacao' => 'required|menor_que:'.Input::get('fim_pontuacao').'|intervalo_pontos_ja_existe_emails:'.Input::get('programa_campanha_id').','.Input::get('tipo').','.$id.'|contem_intervalo_pontos_emails:'.Input::get('programa_campanha_id').','.Input::get('fim_pontuacao').','.Input::get('tipo').','.$id,
				'fim_pontuacao' => 'required|maior_que:'.Input::get('inicio_pontuacao').'|intervalo_pontos_ja_existe_emails:'.Input::get('programa_campanha_id').','.Input::get('tipo').','.$id.'|contem_intervalo_pontos_emails:'.Input::get('programa_campanha_id').','.Input::get('inicio_pontuacao').','.Input::get('tipo').','.$id,
				'programa_campanha_id' => 'required|numeric'
			),
			array(
			    'menor_que' => 'O início da faixa de Pontuação deve ser menor que o término',
			    'maior_que' => 'O término da da faixa de Pontuação deve ser maior que o início',
			    'intervalo_pontos_ja_existe_emails' => 'Já existe um email para o intervalo selecionado nessa Campanha',
				'contem_intervalo_pontos_emails' => 'Já existe um email dentro do intervalo selecionado nessa Campanha',
			)
		);

		if ($validator->fails()){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors($validator);
		}

		$object = ProgramaEmails::find($id);
		$object->fill(Input::all());

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Email alterado com sucesso.');
			return Redirect::route('painel.programaemails.index', array('campanha_id' => $object->programa_campanha_id, 'tipo' => $object->tipo));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Email!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProgramaEmails::find($id);
		$campanha_id = $object->programa_campanha_id;
		$tipo = $object->tipo;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Email removido com sucesso.');

		return Redirect::route('painel.programaemails.index', array('campanha_id' => $campanha_id, 'tipo' => $tipo));
	}

}
