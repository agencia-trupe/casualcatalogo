<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ProdutosMateriais;

class ProdutosMateriaisController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros = ProdutosMateriais::ordenado()->get();
		$this->layout->content = View::make('backend.produtosmateriais.index')->with(compact('registros'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.produtosmateriais.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(!$this->slugDisponivel()){
			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Já existe um Material cadastrado com o título <strong>'.Input::get('titulo').'</strong>.'));
		}

		$object = new ProdutosMateriais;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug($object->titulo);

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Material criado com sucesso.');
			return Redirect::route('painel.produtosmateriais.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Material!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.produtosmateriais.edit')->with('registro', ProdutosMateriais::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(!$this->slugDisponivel($id)){
			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Já existe um Material cadastrado com o título <strong>'.Input::get('titulo').'</strong>.'));
		}

		$object = ProdutosMateriais::find($id);		

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug($object->titulo);

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Material alterado com sucesso.');
			return Redirect::route('painel.produtosmateriais.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Material!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProdutosMateriais::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Material removido com sucesso.');

		return Redirect::route('painel.produtosmateriais.index');
	}

	/**
	 * Método para criação de slug única para materiais
	 *
	 * @return String
	 */
	private function slugDisponivel($material_atual_id = false)
	{
		$titulo  		= Input::get('titulo');
		$slug 			= Str::slug($titulo);
		
		// Checando se existe a mesma para outro material
		if($material_atual_id !== false)
			$query = ProdutosMateriais::slug($slug)->notThis($material_atual_id)->get();
		else
			$query = ProdutosMateriais::slug($slug)->get();

		if(sizeof($query) == 0){
			// Slug disponível
			return true;
		}else{
			// Slug indisponível
			return false;
		}
	}
}