<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ProgramaLojas;

class ProgramaLojasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.programalojas.index')->with('registros', ProgramaLojas::paginate(25));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.programalojas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ProgramaLojas;

		$object->titulo = Input::get('titulo');
		$object->email = Input::get('email');
		$object->endereco = Input::get('endereco');




		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Loja criada com sucesso.');
			return Redirect::route('painel.programalojas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Loja!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.programalojas.edit')->with('registro', ProgramaLojas::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ProgramaLojas::find($id);

		$object->titulo = Input::get('titulo');
		$object->email = Input::get('email');
		$object->endereco = Input::get('endereco');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Loja alterada com sucesso.');
			return Redirect::route('painel.programalojas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Loja!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ProgramaLojas::find($id);

		// Achar participantes que são dessa loja e desativar
		$part = \UsuariosCatalogo::where('programa_lojas_id', '=', $object->id)->get();
		foreach ($part as $key => $value) {
			$value->participante_relacionamento = 0;
			$value->save();
		}

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Loja removida com sucesso.');

		return Redirect::route('painel.programalojas.index');
	}

}