<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \BlogPosts, \BlogCategorias, \BlogTags;

class BlogPostsController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$listaCategorias = BlogCategorias::ordenado()->get();

		if(Input::has('blog_categorias_id')){
			$filtro = Input::get('blog_categorias_id');
			$registros = BlogPosts::ordenado()->filtrarCategoria($filtro)->paginate(25);
		}
		else{
			$filtro = false;
			$registros = BlogPosts::ordenado()->paginate(25);
		}

		$this->layout->content = View::make('backend.blogposts.index')->with(compact('registros'))
																	  ->with(compact('filtro'))
																	  ->with(compact('listaCategorias'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$listaTags = BlogTags::all();
		$listaCategorias = BlogCategorias::ordenado()->get();
		$filtro = Input::get('blog_categorias_id');

		$this->layout->content = View::make('backend.blogposts.form')->with(compact('listaCategorias'))
																	 ->with(compact('filtro'))
																	 ->with(compact('listaTags'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new BlogPosts;

		$object->blog_categorias_id = Input::get('blog_categorias_id');

		$object->titulo = Input::get('titulo');
		$object->data = Tools::converteData(Input::get('data'));
		$object->texto = Input::get('texto');
		$object->publicar = Input::get('publicar');

		$capa = Thumb::make('imagem', 800, null, 'blog/');
		if($capa){
			$object->capa = $capa;
			Thumb::makeFromFile('assets/images/blog/', $capa, 440, 440, 'assets/images/blog/chamadas/');
		}

		$this->armazenarNovasTags();

		$tags_string = Input::get('tags');
		$tags_array = array();
		$tags_id = array();
		// Se só houver uma tag (sem vírgula), adicionar ela em um array
		// para poder fazer o foreach como se houvesse mais de uma
		if(strpos($tags_string, ',') === false && $tags_string != ''){
			array_unshift($tags_array, $tags_string);
		}elseif($tags_string != ''){
			$tags_array = explode(',', $tags_string);
		}else{
			$tags_array = false;
		}

		if($tags_array !== false){
			foreach ($tags_array as $value) {
				$tags_armazenada = BlogTags::slug(Str::slug($value))->first();
				if($tags_armazenada && $tags_armazenada->id)
					$tags_id[] = $tags_armazenada->id;
			}
		}
		try {

			$object->save();

			if($tags_array !== false)
				$object->tags()->sync($tags_id);
			$object->slug = $this->gerarSlug($object->id);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Post criado com sucesso.');
			return Redirect::route('painel.blogposts.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Post!'.$e->getMessage()));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$listaTags = BlogTags::all();
		$listaCategorias = BlogCategorias::ordenado()->get();

		$this->layout->content = View::make('backend.blogposts.edit')->with('registro', BlogPosts::find($id))
																	 ->with(compact('listaCategorias'))
																	 ->with(compact('listaTags'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = BlogPosts::find($id);

		$object->blog_categorias_id = Input::get('blog_categorias_id');

		$object->titulo = Input::get('titulo');
		$object->slug = $this->gerarSlug($id);
		$object->data = Tools::converteData(Input::get('data'));
		$object->texto = Input::get('texto');
		$object->publicar = Input::get('publicar');

		$capa = Thumb::make('imagem', 800, null, 'blog/');
		if($capa){
			$object->capa = $capa;
			Thumb::makeFromFile('assets/images/blog/', $capa, 440, 440, 'assets/images/blog/chamadas/');
		}

		if(Input::has('remover_imagem') && Input::get('remover_imagem') == 1)
			$object->capa = '';

		$this->armazenarNovasTags($id);

		$tags_string = Input::get('tags');
		$tags_array = array();
		$tags_id = array();
		// Se só houver uma tag (sem vírgula), adicionar ela em um array
		// para poder fazer o foreach como se houvesse mais de uma
		if(strpos($tags_string, ',') === false && $tags_string != ''){
			array_unshift($tags_array, $tags_string);
		}elseif($tags_string != ''){
			$tags_array = explode(',', $tags_string);
		}else{
			$tags_array = false;
		}

		if($tags_array !== false){
			foreach ($tags_array as $value) {
				$tags_armazenada = BlogTags::slug(Str::slug($value))->first();
				if($tags_armazenada && $tags_armazenada->id)
					$tags_id[] = $tags_armazenada->id;
			}
		}

		if($tags_array !== false)
			$object->tags()->sync($tags_id);

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Post alterado com sucesso.');
			return Redirect::route('painel.blogposts.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar Post!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = BlogPosts::find($id);

		//$object->comentarios()->delete();
		//$object->tags()->detach();

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Post removido com sucesso.');

		return Redirect::route('painel.blogposts.index');
	}

	/*
	* Inserção de Novas Tags
	*/
	private function armazenarNovasTags()
	{
		$tags = Input::get('tags');
		if($tags == '') return false;

		$xpd = explode(',', $tags);
		foreach ($xpd as $key => $value) {
			$slug = Str::slug($value);
			if(sizeof(BlogTags::slug($slug)->get()) == 0){
				$bt = new BlogTags;
				$bt->titulo = $value;
				$bt->slug = $slug;
				$bt->save();
			}
		}
	}

	private function gerarSlug($post_atual_id)
	{
		$titulo  		= Input::get('titulo');
		$slug 			= Str::slug($titulo);
		$slug_check		= '';
		$append_digito 	= 1;
		$append_string  = '-';

		// Check for matching project slug and client id
		$query = BlogPosts::slug($slug)->notThis($post_atual_id)->get();

		if(sizeof($query) == 0){
			// If there's no matching slugs for the same client,
			// just return the Slugified Title
			return $slug;
		}else{
			// While the current slug exists, do
			while(sizeof($query) > 0){

				// Append the digit to the current slug
				$slug_check = $slug . $append_string . $append_digito;

				// Check again
				$query = BlogPosts::slug($slug_check)->notThis($post_atual_id)->get();

				// Increment the digit for the next iterarion
				$append_digito++;
			}
			// Return appended slug
			return $slug_check;
		}
	}
}