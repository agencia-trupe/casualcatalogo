<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \BlogCategorias;

class BlogCategoriasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.blogcategorias.index')->with('registros', BlogCategorias::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.blogcategorias.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new BlogCategorias;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(BlogCategorias::slug($object->slug)->get()) > 0){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array("Já existe uma categoria no blog com o título <strong>".Input::get('titulo')."</strong>"));
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria criada com sucesso.');
			return Redirect::route('painel.blogcategorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.blogcategorias.edit')->with('registro', BlogCategorias::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = BlogCategorias::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		if(sizeof(BlogCategorias::slug($object->slug)->get()) > 0){
			return Redirect::back()->withErrors(array("Já existe uma categoria no blog com o título <strong>".Input::get('titulo')."</strong>"));
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Categoria alterada com sucesso.');
			return Redirect::route('painel.blogcategorias.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Categoria!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = BlogCategorias::find($id);

		// if(count($object->posts)){
		// 	foreach ($object->posts as $post) {
		// 		$post->comentarios()->delete();
		// 		$post->tags()->detach();
		// 		$post->delete();
		// 	}
		// }

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria removida com sucesso.');

		return Redirect::route('painel.blogcategorias.index');
	}

}