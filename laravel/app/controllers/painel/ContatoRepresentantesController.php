<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ContatoRepresentantes;

class ContatoRepresentantesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.contatorepresentantes.index')->with('registros', ContatoRepresentantes::ordenado()->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.contatorepresentantes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ContatoRepresentantes;

		$object->titulo = Input::get('titulo');
		$object->telefone = Input::get('telefone');
		$object->endereco = Input::get('endereco');
		$object->link_maps = Input::get('link_maps');
		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Representante criado com sucesso.');
			return Redirect::route('painel.contatorepresentantes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Representante!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.contatorepresentantes.edit')->with('registro', ContatoRepresentantes::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ContatoRepresentantes::find($id);

		$object->titulo = Input::get('titulo');
		$object->telefone = Input::get('telefone');
		$object->endereco = Input::get('endereco');
		$object->link_maps = Input::get('link_maps');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Representante alterado com sucesso.');
			return Redirect::route('painel.contatorepresentantes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Representante!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ContatoRepresentantes::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Representante removido com sucesso.');

		return Redirect::route('painel.contatorepresentantes.index');
	}

}