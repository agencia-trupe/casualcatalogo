<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Fornecedores, \FornecedoresArquivos;

class FornecedoresController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$filtro = Input::get('filtro');

		if($filtro == 'interiores' || $filtro == 'exteriores')
			$registros = Fornecedores::{$filtro}()->ordenado()->get();
		else
			$registros = Fornecedores::ordenado()->get();

		$this->layout->content = View::make('backend.fornecedores.index')->with(compact('registros'))
																		 ->with(compact('filtro'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.fornecedores.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Fornecedores;

		$object->divisao = Input::get('divisao');
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->obs = Input::get('obs');
		$object->publicar_site = Input::get('publicar_site');
		$object->publicar_catalogo = Input::get('publicar_catalogo');

		if(!$this->slugDisponivel()){
			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Já existe um Fornecedor de <strong>'.$object->divisao.'</strong> cadastrado com o nome <strong>'.$object->titulo.'</strong>.'));
		}

		$imagem = Thumb::make('imagem', 120, 50, 'fornecedores/', false, '#ffffff', false);
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			$arquivos = Input::get('arquivo');
			$titulos_arquivos = Input::get('titulos_arquivos');

			if($arquivos && is_array($arquivos)){
				foreach ($arquivos as $key => $value) {
					$obj = new FornecedoresArquivos;
					$obj->fornecedores_id = $object->id;
					$obj->titulo = isset($titulos_arquivos[$key]) ? $titulos_arquivos[$key] : '';
					$obj->imagem = $value;
					$obj->ordem = $key;
					$obj->save();
				}
			}

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Fornecedor criado com sucesso.');
			return Redirect::route('painel.fornecedores.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Fornecedor! '.$e->getMessage()));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.fornecedores.edit')->with('registro', Fornecedores::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Fornecedores::find($id);

		$object->divisao = Input::get('divisao');
		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));
		$object->obs = Input::get('obs');
		$object->publicar_site = Input::get('publicar_site');
		$object->publicar_catalogo = Input::get('publicar_catalogo');

		if(!$this->slugDisponivel($id)){
			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Já existe um Fornecedor de <strong>'.$object->divisao.'</strong> cadastrado com o nome <strong>'.$object->titulo.'</strong>.'));
		}

		$imagem = Thumb::make('imagem', 120, 50, 'fornecedores/', false, '#ffffff', false);
		if($imagem) $object->imagem = $imagem;

		FornecedoresArquivos::where('fornecedores_id', '=', $object->id)->delete();

		$arquivos = Input::get('arquivo');
		$titulos_arquivos = Input::get('titulos_arquivos');

		if($arquivos && is_array($arquivos)){
			foreach ($arquivos as $key => $value) {
				$obj = new FornecedoresArquivos;
				$obj->fornecedores_id = $object->id;
				$obj->titulo = isset($titulos_arquivos[$key]) ? $titulos_arquivos[$key] : '';
				$obj->imagem = $value;
				$obj->ordem = $key;
				$obj->save();
			}
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Fornecedor alterado com sucesso.');
			return Redirect::route('painel.fornecedores.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Fornecedor! '.$e->getMessage()));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Fornecedores::find($id);

		// if(count($object->produtos)){
		// 	foreach ($object->produtos as $produto) {
		// 		$produto->arquivos()->delete();
		// 		$produto->imagens()->delete();
		// 		$produto->materiais()->detach();
		// 		$produto->tags()->detach();
		// 		$produto->cores()->detach();
		// 		$produto->delete();
		// 	}
		// }

		// if(count($object->arquivos)){
		// 	foreach ($object->arquivos as $arquivo) {
		// 		$arquivo->delete();
		// 	}
		// }

		// if(count($object->linhas)){
		// 	foreach ($object->linhas as $linha) {
		// 		$linha->delete();
		// 	}
		// }

		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Fornecedor removido com sucesso.');

		return Redirect::route('painel.fornecedores.index');
	}

	/**
	 * Método para criação de slug única para representantes
	 * dentro de uma mesma divisão interiores|exteriores
	 *
	 * @return String
	 */
	public function slugDisponivel($fornecedor_atual_id = false)
	{
		$divisao	 	= Input::get('divisao');
		$titulo  		= Input::get('titulo');
		$slug 			= Str::slug($titulo);

		// Checando se existe a mesma para outro fornecedor
		if($fornecedor_atual_id)
			$query = Fornecedores::{$divisao}()->slug($slug)->notThis($fornecedor_atual_id)->get();
		else
			$query = Fornecedores::{$divisao}()->slug($slug)->get();

		if(sizeof($query) == 0){
			// Slug disponível
			return true;
		}else{
			// Slug indisponível
			return false;
		}
	}
}