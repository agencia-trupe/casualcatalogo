<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Perfil;

class PerfilController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.perfil.index')->with('registros', Perfil::all())->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.perfil.form')->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Perfil;

		$object->missao = Input::get('missao');
		$object->visao = Input::get('visao');
		$object->valores = Input::get('valores');
		$imagem = Thumb::make('imagem', 540, 340, 'perfil/');
		if($imagem) $object->imagem = $imagem;

		if($this->limiteInsercao && sizeof( Perfil::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Perfil criado com sucesso.');
			return Redirect::route('painel.perfil.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Perfil!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.perfil.edit')->with('registro', Perfil::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Perfil::find($id);

		$object->missao = Input::get('missao');
		$object->visao = Input::get('visao');
		$object->valores = Input::get('valores');

		$imagem = Thumb::make('imagem', 540, 340, 'perfil/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Perfil alterado com sucesso.');
			return Redirect::route('painel.perfil.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Perfil!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::route('painel.perfil.index');
	}

}