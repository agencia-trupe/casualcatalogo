<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File;
use \InstitucionalFornecedores;

class InstitucionalFornecedoresController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$registros = InstitucionalFornecedores::ordenado()->get();
		$this->layout->content = View::make('backend.institucional-fornecedores.index')->with(compact('registros'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.institucional-fornecedores.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new InstitucionalFornecedores;

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', null, 60, 'fornecedores/institucional/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Fornecedor criado com sucesso.');
			return Redirect::route('painel.institucional-fornecedores.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Fornecedor!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.institucional-fornecedores.edit')->with('registro', InstitucionalFornecedores::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = InstitucionalFornecedores::find($id);

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', null, 60, 'fornecedores/institucional/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Fornecedor alterado com sucesso.');
			return Redirect::route('painel.institucional-fornecedores.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Fornecedor!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = InstitucionalFornecedores::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Fornecedor removido com sucesso.');

		return Redirect::route('painel.institucional-fornecedores.index');
	}

}