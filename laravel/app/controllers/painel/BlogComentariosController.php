<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \BlogComentarios, \BlogPosts;

class BlogComentariosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(!Input::has('blog_posts_id')) return Redirect::route('painel.blogposts.index');

		$post = BlogPosts::find(Input::get('blog_posts_id'));

		if(!$post) return Redirect::route('painel.blogposts.index');

		$comentarios = $post->comentarios()->paginate(30);

		$this->layout->content = View::make('backend.blogcomentarios.index')->with(compact('post'))
																			->with(compact('comentarios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.blogposts.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.blogposts.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$registro = BlogComentarios::find($id);
		$this->layout->content = View::make('backend.blogcomentarios.edit')->with(compact('registro'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = BlogComentarios::find($id);

		$object->aprovado = Input::get('aprovado');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Comentário alterado com sucesso.');
			return Redirect::route('painel.blogcomentarios.index', array('blog_posts_id' => $object->blog_posts_id));

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar Comentário!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = BlogComentarios::find($id);
		$blog_posts_id = $object->blog_posts_id;
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Comentário removido com sucesso.');

		return Redirect::route('painel.blogcomentarios.index', array('blog_posts_id' => $blog_posts_id));
	}

}