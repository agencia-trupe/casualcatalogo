<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \ProdutosDestaques, \Produtos, \Fornecedores;

class DestaquesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.destaques.index')->with('registros', ProdutosDestaques::all())
																	  ->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$fornecedores_int = Fornecedores::interiores()->ordenado()->get();
		$fornecedores_ext = Fornecedores::exteriores()->ordenado()->get();

		$this->layout->content = View::make('backend.destaques.form')->with(compact('fornecedores_int'))
																	 ->with(compact('fornecedores_ext'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ProdutosDestaques;

		$object->interiores_1 = Input::get('interiores_1');
		$object->interiores_2 = Input::get('interiores_2');
		$object->interiores_3 = Input::get('interiores_3');

		$object->exteriores_1 = Input::get('exteriores_1');
		$object->exteriores_2 = Input::get('exteriores_2');
		$object->exteriores_3 = Input::get('exteriores_3');

		if($this->limiteInsercao && sizeof( ProdutosDestaques::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Destaques criados com sucesso.');
			return Redirect::route('painel.destaques.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Destaques! É necessário especificar o Produto do Fornecedor.'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$fornecedores_int = Fornecedores::interiores()->ordenado()->get();
		$fornecedores_ext = Fornecedores::exteriores()->ordenado()->get();
		$registro = ProdutosDestaques::find($id);

		$this->layout->content = View::make('backend.destaques.edit')->with(compact('registro'))
																	 ->with(compact('fornecedores_int'))
																	 ->with(compact('fornecedores_ext'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ProdutosDestaques::find($id);

		$object->interiores_1 = Input::get('interiores_1');
		$object->interiores_2 = Input::get('interiores_2');
		$object->interiores_3 = Input::get('interiores_3');

		$object->exteriores_1 = Input::get('exteriores_1');
		$object->exteriores_2 = Input::get('exteriores_2');
		$object->exteriores_3 = Input::get('exteriores_3');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Destaques alterados com sucesso.');
			return Redirect::route('painel.destaques.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar Destaques! É necessário especificar o Produto do Fornecedor.'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::route('painel.destaques.index');
	}

}