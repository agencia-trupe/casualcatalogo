<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \SiteImagemHome;

class SiteImagemHomeController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.siteimagemhome.index')->with('registros', SiteImagemHome::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.siteimagemhome.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new SiteImagemHome;

		$imagem = Thumb::make('imagem', 440, 440, 'siteimagemhome/');
		if($imagem) $object->imagem = $imagem;

		$object->link = Input::get('link');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criada com sucesso.');
			return Redirect::route('painel.siteimagenshome.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.siteimagemhome.edit')->with('registro', SiteImagemHome::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = SiteImagemHome::find($id);

		$imagem = Thumb::make('imagem', 440, 440, 'siteimagemhome/');
		if($imagem) $object->imagem = $imagem;

		$object->link = Input::get('link');

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterada com sucesso.');
			return Redirect::route('painel.siteimagenshome.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::except('imagem'));
			return Redirect::back()->withErrors(array('Erro ao criar Imagem!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$obj = SiteImagemHome::find($id);
		$obj->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removida com sucesso.');

		return Redirect::route('painel.siteimagenshome.index');
	}

}