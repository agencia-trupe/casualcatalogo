<?php

namespace Site\Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Tipos;

class TiposController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.tipos.index')->with('registros', Tipos::ordenado()->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.tipos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Tipos;

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		$check_slug = Tipos::slug($object->slug)->get();

		if(sizeof($check_slug) > 0){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Esse tipo de produto já está cadastrado!'));
		}

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Tipo de Produto criado com sucesso.');
			return Redirect::route('painel.tipos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Tipo de Produto!'));

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.tipos.edit')->with('registro', Tipos::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Tipos::find($id);

		$object->titulo = Input::get('titulo');
		$object->slug = Str::slug(Input::get('titulo'));

		$check_slug = Tipos::slug($object->slug)->notThis($id)->get();

		if(sizeof($check_slug) > 0){
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Esse tipo de produto já está cadastrado!'));
		}

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Tipo de Produto alterado com sucesso.');
			return Redirect::route('painel.tipos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Tipo de Produto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Tipos::find($id);
		if(count($object->produtos)){
			foreach ($object->produtos as $produto) {
				$produto->arquivos()->delete();
				$produto->imagens()->delete();
				$produto->materiais()->detach();
				$produto->tags()->detach();
				$produto->cores()->detach();
				$produto->delete();
			}
		}
		$object->delete();


		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Tipo de Produto removido com sucesso.');

		return Redirect::route('painel.tipos.index');
	}

}