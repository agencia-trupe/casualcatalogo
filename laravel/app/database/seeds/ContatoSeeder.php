<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
			    'facebook' 	=> '',
                'twitter' 	=> '',
                'flickr' 	=> '',
                'instagram' => '',
            )
        );

        DB::table('contato')->insert($data);
    }

}
