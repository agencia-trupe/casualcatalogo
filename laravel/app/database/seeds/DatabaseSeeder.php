<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersPainelSeeder');
		$this->call('PerfilSeeder');
		$this->call('ContatoSeeder');
		$this->call('LinhasSeeder');
		$this->call('DestaquesSeeder');
	}

}
