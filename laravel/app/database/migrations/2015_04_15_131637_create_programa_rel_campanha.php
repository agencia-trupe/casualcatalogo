<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaRelCampanha extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('programa_campanha', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->date('data_inicio');
			$table->date('data_termino');
			$table->text('regulamento_arquiteto');
			$table->text('regulamento_assistente');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('programa_campanha');
	}

}
