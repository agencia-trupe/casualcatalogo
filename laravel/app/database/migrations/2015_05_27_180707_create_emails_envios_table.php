<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsEnviosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('programa_envios_emails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('programa_campanha_id')->unsigned();
			$table->integer('programa_participante_id')->unsigned();
			$table->text('mensagem');
			$table->integer('enviado')->default(0);
			$table->date('data_criacao_lista');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('programa_envios_emails');
	}

}
