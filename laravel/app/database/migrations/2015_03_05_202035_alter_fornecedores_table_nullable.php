<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFornecedoresTableNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		\DB::statement('ALTER TABLE `fornecedores` MODIFY `publicar_site` INTEGER NULL;');
		\DB::statement('UPDATE `fornecedores` SET `publicar_site` = NULL WHERE `publicar_site` = 0;');

		\DB::statement('ALTER TABLE `fornecedores` MODIFY `publicar_catalogo` INTEGER NULL;');
		\DB::statement('UPDATE `fornecedores` SET `publicar_catalogo` = NULL WHERE `publicar_catalogo` = 0;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		\DB::statement('UPDATE `fornecedores` SET `publicar_site` = 0 WHERE `publicar_site` IS NULL;');
    	\DB::statement('ALTER TABLE `fornecedores` MODIFY `publicar_site` INTEGER NOT NULL;');

    	\DB::statement('UPDATE `fornecedores` SET `publicar_catalogo` = 0 WHERE `publicar_catalogo` IS NULL;');
    	\DB::statement('ALTER TABLE `fornecedores` MODIFY `publicar_catalogo` INTEGER NOT NULL;');
	}

}
