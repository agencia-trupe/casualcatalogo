<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProdutosCoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produtos_cores', function(Blueprint $table)
		{
			$table->string('representacao')->after('slug');
			$table->string('hexa', 7)->after('imagem');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos_cores', function(Blueprint $table)
		{
			$table->dropColumn('representacao');
			$table->dropColumn('hexa');
		});
	}

}
