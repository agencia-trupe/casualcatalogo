<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysDestaques extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::table('produtos_destaques', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos_destaques` MODIFY `interiores_1` INTEGER UNSIGNED NULL;');
			$table->foreign('interiores_1')->references('id')->on('produtos')->onDelete('SET NULL');

			DB::statement('ALTER TABLE `produtos_destaques` MODIFY `interiores_2` INTEGER UNSIGNED NULL;');
			$table->foreign('interiores_2')->references('id')->on('produtos')->onDelete('SET NULL');

			DB::statement('ALTER TABLE `produtos_destaques` MODIFY `interiores_3` INTEGER UNSIGNED NULL;');
			$table->foreign('interiores_3')->references('id')->on('produtos')->onDelete('SET NULL');

			DB::statement('ALTER TABLE `produtos_destaques` MODIFY `exteriores_1` INTEGER UNSIGNED NULL;');
			$table->foreign('exteriores_1')->references('id')->on('produtos')->onDelete('SET NULL');

			DB::statement('ALTER TABLE `produtos_destaques` MODIFY `exteriores_2` INTEGER UNSIGNED NULL;');
			$table->foreign('exteriores_2')->references('id')->on('produtos')->onDelete('SET NULL');

			DB::statement('ALTER TABLE `produtos_destaques` MODIFY `exteriores_3` INTEGER UNSIGNED NULL;');
			$table->foreign('exteriores_3')->references('id')->on('produtos')->onDelete('SET NULL');

		});

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos_destaques', function(Blueprint $table)
		{
			$table->dropForeign(['interiores_1']);
			$table->dropForeign(['interiores_2']);
			$table->dropForeign(['interiores_3']);
			$table->dropForeign(['exteriores_1']);
			$table->dropForeign(['exteriores_2']);
			$table->dropForeign(['exteriores_3']);
		});
	}

}
