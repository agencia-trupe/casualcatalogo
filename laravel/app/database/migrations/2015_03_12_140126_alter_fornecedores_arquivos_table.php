<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFornecedoresArquivosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fornecedores_arquivos', function(Blueprint $table)
		{
			$table->string('titulo')->after('fornecedores_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fornecedores_arquivos', function(Blueprint $table)
		{
			$table->string('dropColumns');
		});
	}

}
