<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProgramaRelUsuariosTipo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuarios_catalogo', function(Blueprint $table)
		{
			$table->integer('participante_relacionamento')->after('programa_lojas_id');
			$table->string('tipo_participacao_relacionamento')->after('participante_relacionamento');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuarios_catalogo', function(Blueprint $table)
		{
			$table->dropColumn('participante_relacionamento');
			$table->dropColumn('tipo_participacao_relacionamento');
		});
	}

}
