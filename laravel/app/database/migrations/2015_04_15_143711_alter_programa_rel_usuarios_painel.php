<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProgramaRelUsuariosPainel extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuarios_painel', function(Blueprint $table)
		{
			$table->integer('programa_lojas_id')->after('remember_token')->unsigned()->nullable();
			$table->foreign('programa_lojas_id')->references('id')->on('programa_lojas')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuarios_painel', function(Blueprint $table)
		{
			$table->dropForeign('usuarios_painel_programa_lojas_id_foreign');
			$table->dropColumn('programa_lojas_id');
		});
	}

}
