<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProgramaPremios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('programa_premios', function(Blueprint $table)
		{
			$table->string('titulo')->after('id');
			$table->text('texto')->after('titulo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('programa_premios', function(Blueprint $table)
		{
			$table->dropColumn('titulo');
			$table->dropColumn('texto');
		});
	}

}
