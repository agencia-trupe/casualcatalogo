<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysPrograma extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::table('programa_envios_emails', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `programa_envios_emails` MODIFY `programa_campanha_id` INTEGER UNSIGNED NULL;');
			$table->foreign('programa_campanha_id')->references('id')->on('programa_campanha')->onDelete('CASCADE');

			DB::statement('ALTER TABLE `programa_envios_emails` MODIFY `programa_participante_id` INTEGER UNSIGNED NULL;');
			$table->foreign('programa_participante_id')->references('id')->on('usuarios_catalogo')->onDelete('CASCADE');
		});
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('programa_envios_emails', function(Blueprint $table)
		{
			$table->dropForeign(['programa_campanha_id']);
			$table->dropForeign(['programa_participante_id']);
		});
	}

}
