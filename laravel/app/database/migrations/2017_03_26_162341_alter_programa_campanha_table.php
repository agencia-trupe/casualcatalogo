<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlterProgramaCampanhaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('programa_campanha', function(Blueprint $table)
		{
			$table->string('titulo_en');
			$table->text('regulamento_arquiteto_en');
			$table->text('regulamento_assistente_en');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('programa_campanha', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('regulamento_arquiteto_en');
			$table->dropColumn('regulamento_assistente_en');
		});
	}

}
