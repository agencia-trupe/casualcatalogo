<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFornecedoresInstitucionalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fornecedores_institucional', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('imagem');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fornecedores_institucional');
	}

}

/*
create table `fornecedores_institucional` (`id` int unsigned not null auto_increment primary key, `titulo` varchar(255) not null, `imagem` varchar(255) not null, `ordem` int not null, `created_at` timestamp default 0 not null, `updated_at` timestamp default 0 not null) default character set utf8 collate utf8_unicode_ci
*/