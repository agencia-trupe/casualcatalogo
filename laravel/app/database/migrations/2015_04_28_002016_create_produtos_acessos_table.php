<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosAcessosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produtos_acessos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('produtos_id')->unsigned()->nullable();
			$table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('CASCADE');
			$table->integer('total');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produtos_acessos');
	}

}
