<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsuariosTableGerentes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuarios_painel', function(Blueprint $table)
		{
			$table->integer('is_gerente')->default(0)->after('assist_programa'); // 0 | 1
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuarios_painel', function(Blueprint $table)
		{
			$table->dropColumn('is_gerente');
		});
	}

}
