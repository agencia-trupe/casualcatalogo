<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produtos_tags', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo', 250)->nullable();
			$table->string('slug', 250)->nullable();
			$table->integer('ordem')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produtos_tags');
	}

}
