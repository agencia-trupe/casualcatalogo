<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPontuacaoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('programa_pontuacao', function(Blueprint $table)
		{
			$table->softDeletes();
			$table->string('deleted_by')->after('deleted_at');
			$table->date('data_insercao')->after('pontos');
			$table->integer('usuarios_painel_id')->after('pontos')->unsigned()->nullable();
			$table->foreign('usuarios_painel_id')->references('id')->on('usuarios_painel')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('programa_pontuacao', function(Blueprint $table)
		{
			$table->dropForeign(['usuarios_painel_id']);
			$table->dropColumn('usuarios_painel_id');
			$table->dropColumn('data_insercao');
			$table->dropColumn('deleted_by');
			$table->dropColumn('deleted_at');
		});
	}

}
