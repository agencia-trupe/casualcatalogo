<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramaRelChamadas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('programa_chamadas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('programa_campanha_id')->unsigned();
			$table->foreign('programa_campanha_id')->references('id')->on('programa_campanha')->onDelete('cascade');
			$table->integer('inicio_pontuacao');
			$table->integer('fim_pontuacao');
			$table->text('texto');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('programa_chamadas');
	}

}
