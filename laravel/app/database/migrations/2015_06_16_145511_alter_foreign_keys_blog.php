<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysBlog extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::table('blog_posts', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `blog_posts` MODIFY `blog_categorias_id` INTEGER UNSIGNED NULL;');
			$table->foreign('blog_categorias_id')->references('id')->on('blog_categorias')->onDelete('CASCADE');
		});
		Schema::table('blog_comentarios', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `blog_comentarios` MODIFY `blog_posts_id` INTEGER UNSIGNED NULL;');
			$table->foreign('blog_posts_id')->references('id')->on('blog_posts')->onDelete('CASCADE');
		});
		Schema::table('blog_tags_has_blog_posts', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `blog_tags_has_blog_posts` MODIFY `blog_tags_id` INTEGER UNSIGNED NULL;');
			$table->foreign('blog_tags_id')->references('id')->on('blog_tags')->onDelete('CASCADE');

			DB::statement('ALTER TABLE `blog_tags_has_blog_posts` MODIFY `blog_posts_id` INTEGER UNSIGNED NULL;');
			$table->foreign('blog_posts_id')->references('id')->on('blog_posts')->onDelete('CASCADE');
		});
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('blog_posts', function(Blueprint $table)
		{
			$table->dropForeign(['blog_categorias_id']);
		});
		Schema::table('blog_comentarios', function(Blueprint $table)
		{
			$table->dropForeign(['blog_posts_id']);
		});
		Schema::table('blog_tags_has_blog_posts', function(Blueprint $table)
		{
			$table->dropForeign(['blog_tags_id']);
			$table->dropForeign(['blog_posts_id']);
		});
	}

}
