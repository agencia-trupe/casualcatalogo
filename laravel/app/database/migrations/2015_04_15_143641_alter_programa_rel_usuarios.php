<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProgramaRelUsuarios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('usuarios_catalogo', function(Blueprint $table)
		{
			$table->datetime('ultima_atividade')->after('remember_token');
			$table->integer('programa_lojas_id')->after('ultima_atividade')->unsigned()->nullable();
			$table->foreign('programa_lojas_id')->references('id')->on('programa_lojas')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('usuarios_catalogo', function(Blueprint $table)
		{
			$table->dropForeign('usuarios_catalogo_programa_lojas_id_foreign');
			$table->dropColumn(array('programa_lojas_id', 'ultima_atividade'));
		});
	}

}
