<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignKeysProdutos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		Schema::table('produtos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos` MODIFY `linhas_id` INTEGER UNSIGNED NULL;');
			$table->foreign('linhas_id')->references('id')->on('linhas')->onDelete('CASCADE');

			DB::statement('ALTER TABLE `produtos` MODIFY `tipos_id` INTEGER UNSIGNED NULL;');
			$table->foreign('tipos_id')->references('id')->on('tipos')->onDelete('CASCADE');
		});

		Schema::table('produtos_arquivos', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos_arquivos` MODIFY `produtos_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('CASCADE');
		});

		Schema::table('produtos_imagens', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos_imagens` MODIFY `produtos_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('CASCADE');
		});

		Schema::table('produtos_has_produtos_cores', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos_has_produtos_cores` MODIFY `produtos_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('CASCADE');

			DB::statement('ALTER TABLE `produtos_has_produtos_cores` MODIFY `produtos_cores_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_cores_id')->references('id')->on('produtos_cores')->onDelete('CASCADE');
		});

		Schema::table('produtos_has_produtos_tags', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos_has_produtos_tags` MODIFY `produtos_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('CASCADE');

			DB::statement('ALTER TABLE `produtos_has_produtos_tags` MODIFY `produtos_tags_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_tags_id')->references('id')->on('produtos_tags')->onDelete('CASCADE');
		});

		Schema::table('produtos_has_produtos_materiais', function(Blueprint $table)
		{
			DB::statement('ALTER TABLE `produtos_has_produtos_materiais` MODIFY `produtos_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_id')->references('id')->on('produtos')->onDelete('CASCADE');

			DB::statement('ALTER TABLE `produtos_has_produtos_materiais` MODIFY `produtos_materiais_id` INTEGER UNSIGNED NULL;');
			$table->foreign('produtos_materiais_id')->references('id')->on('produtos_materiais')->onDelete('CASCADE');
		});

		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos', function(Blueprint $table)
		{
			$table->dropForeign(['linhas_id']);
			$table->dropForeign(['tipos_id']);
		});
		Schema::table('produtos_arquivos', function(Blueprint $table)
		{
			$table->dropForeign(['produtos_id']);
		});
		Schema::table('produtos_imagens', function(Blueprint $table)
		{
			$table->dropForeign(['produtos_id']);
		});
		Schema::table('produtos_has_produtos_cores', function(Blueprint $table)
		{
			$table->dropForeign(['produtos_id']);
			$table->dropForeign(['produtos_cores_id']);
		});
		Schema::table('produtos_has_produtos_tags', function(Blueprint $table)
		{
			$table->dropForeign(['produtos_id']);
			$table->dropForeign(['produtos_tags_id']);
		});
		Schema::table('produtos_has_produtos_materiais', function(Blueprint $table)
		{
			$table->dropForeign(['produtos_id']);
			$table->dropForeign(['produtos_materiais_id']);
		});
	}

}
