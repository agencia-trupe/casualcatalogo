<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProdutosCodigoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produtos', function(Blueprint $table)
		{
			$table->renameColumn('titulo', 'codigo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos', function(Blueprint $table)
		{
			$table->renameColumn('codigo', 'titulo');
		});
	}

}

/*
ALTER TABLE produtos CHANGE titulo codigo VARCHAR(250) DEFAULT NULL
alter table `produtos` add `titulo` varchar(250) null after `divisao`
*/