<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosHasProdutosTagsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('produtos_has_produtos_tags', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('produtos_id');
			$table->integer('produtos_tags_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('produtos_has_produtos_tags');
	}

}
