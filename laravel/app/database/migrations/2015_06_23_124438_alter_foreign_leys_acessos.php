<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignLeysAcessos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produtos_acessos', function(Blueprint $table)
		{
			$table->foreign('programa_usuario_id')->references('id')->on('usuarios_catalogo')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos_acessos', function(Blueprint $table)
		{
			$table->dropForeign(['programa_usuario_id']);
		});
	}

}
