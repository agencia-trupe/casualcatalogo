<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRepresentantesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contato_representantes', function(Blueprint $table)
		{
			$table->integer('ordem')->after('link_maps');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contato_representantes', function(Blueprint $table)
		{
			$table->dropColumn('ordem');
		});
	}

}
