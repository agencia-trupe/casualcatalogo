@section('conteudo')

	<div class="main" id="main-perfil">

		<div class="coluna-esquerda">
			<div class="visao">
				<div class="titulo">
					<h1>VISÃO</h1>
				</div>
				<div class="texto">{{nl2br($perfil->visao)}}</div>
			</div>
			<div class="valores">
				<div class="titulo">
					<h1>VALORES</h1>
				</div>
				<div class="texto">{{nl2br($perfil->valores)}}</div>
			</div>
			<div class="missao">
				<div class="titulo">
					<h1>MISSÃO</h1>
				</div>
				<div class="texto">{{nl2br($perfil->missao)}}</div>
			</div>
		</div>

		<div class="coluna-direita">
			<img src="assets/images/perfil/{{$perfil->imagem}}" alt="Casual Móveis">
		</div>

	</div>

@stop