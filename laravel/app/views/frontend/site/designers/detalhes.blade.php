@section('conteudo')

	<div class="main marcas-lista" id="main-designers">

		<h1>DESIGNERS</h1>

		<div class="contem-designers">
			<div class="coluna-esquerda">
				<img src="assets/images/designers/{{ $designer->foto }}" alt="{{ $designer->nome }}">
				<h2>{{ $designer->nome }}</h2>
			</div>

			<div class="coluna-central">
				{{ nl2br($designer->texto) }}
			</div>

			<div class="coluna-direita">
				<ul>
				@foreach($designer->imagens as $img)
					<li>
						<a href="assets/images/designers/redimensionadas/{{ $img->imagem }}" class="fancybox" title='ampliar' rel='lightbox-designer'>
							<img src="assets/images/designers/thumbs/{{ $img->imagem }}">
						</a>
					</li>
				@endforeach
				</ul>
			</div>
		</div>

	</div>

@stop