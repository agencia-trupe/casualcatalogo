@section('conteudo')

	<div class="main produtos-lista" id="main-produtos">

		<div id="abas">
			<a href="produtos/{{ $divisao }}" title="Ver Produtos por Tipo" class="hoverable @if($abaAberta == 'produtos') ativo @endif">produtos</a>
			<a href="produtos/{{ $divisao }}/marcas" title="Ver Produtos por Marcas" class="hoverable @if($abaAberta == 'marcas') ativo @endif">marcas</a>
		</div>
		<div id="conteudo">
			<aside>
				<h1>{{ $divisao }}</h1>
				<ul id="listaTipos">
					@if(sizeof($listaLateral))
						@foreach ($listaLateral as $link)
							<li><a href="produtos/{{ $divisao }}/marcas/{{ $link->slug }}" title="{{ $link->titulo }}" class="hoverable linkMarcas @if(isset($fornecedorSelecionado->id) && $fornecedorSelecionado->id == $link->id) ativo @endif">{{ $link->titulo }}</a></li>
						@endforeach
					@endif
				</ul>
			</aside>
			<section>
				@if(sizeof($listaProdutos))
					@foreach ($listaProdutos as $produto)
						<a href="produtos/{{ $divisao }}/marcas/{{ $produto->fornecedor->slug }}/{{ $produto->slug }}" class="hoverable thumb-produto" title="{{ $produto->codigo }}">
							<img src="assets/images/produtos/capas/{{ $produto->imagem_capa }}" alt="{{ $produto->codigo }}">
						</a>
					@endforeach
				@endif
			</section>
		</div>

	</div>

@stop