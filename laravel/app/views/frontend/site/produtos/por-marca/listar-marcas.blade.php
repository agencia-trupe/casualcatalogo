@section('conteudo')

	<div class="main produtos-lista" id="main-produtos">

		<div id="abas">
			<a href="produtos/{{ $divisao }}" title="Ver Produtos por Tipo" class="hoverable @if($abaAberta == 'produtos') ativo @endif">produtos</a>
			<a href="produtos/{{ $divisao }}/marcas" title="Ver Produtos por Marcas" class="hoverable @if($abaAberta == 'marcas') ativo @endif">marcas</a>
		</div>
		<div id="conteudo">
			<aside>
				<h1>{{ $divisao }}</h1>
				<ul id="listaTipos">
					@if(sizeof($listaLateral))
						@foreach ($listaLateral as $link)
							<li><a href="produtos/{{ $divisao }}/marcas/{{ $link->slug }}" title="{{ $link->titulo }}" class="hoverable linkMarcas @if(isset($fornecedorSelecionado->id) && $fornecedorSelecionado->id == $link->id) ativo @endif">{{ $link->titulo }}</a></li>
						@endforeach
					@endif
				</ul>
			</aside>
			<section>
				@if(sizeof($listaLateral))
					@foreach ($listaLateral as $fornecedor)
						<a href="produtos/{{ $divisao }}/marcas/{{ $fornecedor->slug }}" class="hoverable thumb-marca" title="{{ $fornecedor->titulo }}">
							<img src="assets/images/fornecedores/{{ $fornecedor->imagem }}" alt="{{ $fornecedor->titulo }}">
						</a>
					@endforeach
				@endif
			</section>
		</div>

	</div>

@stop