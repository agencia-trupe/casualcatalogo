@section('conteudo')

	<div class="main produtos-detalhes" id="main-produtos">

		<div id="abas">
			<a href="produtos/{{ $divisao }}" title="Ver Produtos por Tipo" class="hoverable @if($abaAberta == 'produtos') ativo @endif">produtos</a>
			<a href="produtos/{{ $divisao }}/marcas" title="Ver Produtos por Marcas" class="hoverable @if($abaAberta == 'marcas') ativo @endif">marcas</a>
		</div>
		<div id="conteudo">
			<aside>
				<h1>{{ $divisao }}</h1>
				<ul id="listaTipos">
					@if(sizeof($listaLateral))
						@foreach ($listaLateral as $tipo)
							<li><a href="produtos/{{ $divisao }}/{{ $tipo->slug }}" title="{{ $tipo->titulo }}" class="hoverable @if(isset($fornecedorSelecionado->id) && $fornecedorSelecionado->id == $tipo->id) ativo @endif">{{ $tipo->titulo }}</a></li>
						@endforeach
					@endif
				</ul>
			</aside>
			<section class="detalhes">

				<div id="imagens">

					<div class="animate">

						<div class="imagem">
							<img src="assets/images/produtos/redimensionadas/{{ $detalheProduto->imagem_capa }}" alt="{{ $detalheProduto->codigo }}">
						</div>

						@if(sizeof($detalheProduto->imagens))
							@foreach ($detalheProduto->imagens as $img)
								<div class="imagem">
									<img src="assets/images/produtos/redimensionadas/{{ $img->imagem }}" alt="{{ $detalheProduto->codigo }}">
								</div>
							@endforeach
						@endif

					</div>

					<div id="controles">
						<a href="#" title="Imagem Anterior" class="prev"></a>
						<a href="#" title="Próxima Imagem" class="next"></a>
					</div>

				</div>

				<div class="fornecedor">
					<img src="assets/images/fornecedores/{{ $detalheProduto->fornecedor->imagem }}" alt="{{ $detalheProduto->fornecedor->titulo }}" title="{{ $detalheProduto->fornecedor->titulo }}">
				</div>

				<h2>{{ $detalheProduto->codigo }}</h2>

				<div id="abas-relacionados" class="relacionados">

					<h3>produtos relacionados</h3>

					<div class="abas">
						@if(sizeof($relacionadosMesmaLinha) > 0)
							<a href="{{Request::url()}}#mesmo-tipo" title="mais opções &bull; {{ $detalheProduto->tipo->titulo }}" class="hoverable aba abrir-aba ativo" data-aba="#mesmo-tipo">
								<span>mais opções &bull; {{ $detalheProduto->tipo->titulo }}</span>
							</a>
							<a href="{{Request::url()}}#mesma-linha" title="produtos da mesma linha" class="hoverable aba abrir-aba" data-aba="#mesma-linha">
								<span>produtos da mesma linha</span>
							</a>
						@else
							<div class="aba ativo">
								<span>mais opções &bull; {{ $detalheProduto->tipo->titulo }}</span>
							</div>
						@endif
						<a href="produtos/{{ $divisao }}/marcas/{{ $detalheProduto->fornecedor->slug }}" title="outros produtos da mesma marca" class="hoverable aba">
							<span>outros produtos da mesma marca</span>
						</a>
					</div>

					<div class="conteudos">
						<div class="conteudo-aba ativo" id="mesmo-tipo">
							@if(sizeof($relacionadosMesmoTipo))
								@foreach ($relacionadosMesmoTipo as $produto)
									<a href="produtos/{{ $divisao }}/marcas/{{ $produto->fornecedor->slug }}/{{ $produto->slug }}" class="hoverable thumb-produto pequena thumb-pormarca" title="{{ $produto->codigo }}">
										<img src="assets/images/produtos/capas/{{ $produto->imagem_capa }}" alt="{{ $produto->codigo }}">
									</a>
								@endforeach
							@endif
						</div>
						<div class="conteudo-aba" id="mesma-linha">
							@if(sizeof($relacionadosMesmaLinha))
								@foreach ($relacionadosMesmaLinha as $produto)
									<a href="produtos/{{ $divisao }}/marcas/{{ $produto->fornecedor->slug }}/{{ $produto->slug }}" class="hoverable thumb-produto pequena thumb-pormarca" title="{{ $produto->codigo }}">
										<img src="assets/images/produtos/capas/{{ $produto->imagem_capa }}" alt="{{ $produto->codigo }}">
									</a>
								@endforeach
							@endif
						</div>
					</div>

				</div>
			</section>
		</div>

	</div>

@stop