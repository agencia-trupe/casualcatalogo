@section('conteudo')

	<div class="main produtos-detalhes" id="main-produtos">

		<div id="abas">
			<a href="produtos/{{ $divisao }}" title="Ver Produtos por Tipo" class="hoverable @if($abaAberta == 'produtos') ativo @endif">produtos</a>
			<div id="busca-codigo">
				<form action="{{ route('produtos.buscar') }}" method="post">
					<input type="text" name="termo" required placeholder="busca (código)">
					<input type="submit" value="buscar">
				</form>
			</div>
		</div>
		<div id="conteudo">
			<aside>
				<h1>{{ $divisao }}</h1>
				<ul id="listaTipos">
					@if(sizeof($listaLateral))
						@foreach ($listaLateral as $tipo)
							<li><a href="produtos/{{ $divisao }}/{{ $tipo->slug }}" title="{{ $tipo->titulo }}" class="hoverable @if(isset($tipoSelecionado->id) && $tipoSelecionado->id == $tipo->id) ativo @endif">{{ $tipo->titulo }}</a></li>
						@endforeach
					@endif
				</ul>
			</aside>
			<section class="detalhes">

				<div id="imagens">

					<div class="animate">

						<div class="imagem">
							<img src="assets/images/produtos/redimensionadas/{{ $detalheProduto->imagem_capa }}" alt="{{ $detalheProduto->codigo }}">
						</div>

						@if(sizeof($detalheProduto->imagens))
							@foreach ($detalheProduto->imagens as $img)
								<div class="imagem">
									<img src="assets/images/produtos/redimensionadas/{{ $img->imagem }}" alt="{{ $detalheProduto->codigo }}">
								</div>
							@endforeach
						@endif

					</div>

					<div id="controles">
						<a href="#" title="Imagem Anterior" class="prev"></a>
						<a href="#" title="Próxima Imagem" class="next"></a>
					</div>

				</div>

				<h2>{{ $detalheProduto->codigo }}</h2>

				<div id="abas-relacionados" class="relacionados">

					<div class="abas">
						<div class="aba ativo">
							<span>mais opções &bull; {{ $detalheProduto->tipo->titulo }}</span>
						</div>
					</div>

					<div class="conteudos">
						<div class="conteudo-aba ativo" id="mesmo-tipo">
							@if(sizeof($relacionadosMesmoTipo))
								@foreach ($relacionadosMesmoTipo as $produto)
									<a href="produtos/{{ $divisao }}/{{ $produto->tipo->slug }}/{{ $produto->slug }}" class="hoverable thumb-produto pequena" title="{{ $produto->codigo }}">
										<img src="assets/images/produtos/capas/{{ $produto->imagem_capa }}" alt="{{ $produto->codigo }}">
									</a>
								@endforeach
							@endif
						</div>
					</div>

				</div>
			</section>
		</div>

	</div>

@stop