@section('conteudo')

	<div class="main" id="main-home">

		<div class="grid-home">
			<div class="superior">
				<div class="esquerda">
					@for ($i = 0; $i <= 5; $i++)
						@if(isset($produtos[$i]))
							@if($produtos[$i]->link != '')
								<a href="{{ $produtos[$i]->link }}" class="hoverable thumb-produto">
									<img src="assets/images/siteimagemhome/{{ $produtos[$i]->imagem }}">
								</a>
							@else
								<div class="thumb-produto">
									<img src="assets/images/siteimagemhome/{{ $produtos[$i]->imagem }}">
								</div>
							@endif
						@endif
					@endfor
				</div>
				<div class="banners">
					<div class="animate">
						@if(sizeof($chamadas))
							@foreach($chamadas as $chamada)
								<a href="{{ $chamada->link }}" title="{{ $chamada->titulo }}" class="banner banner-destaque" @if($chamada->target == 'externo') target='_blank' @endif>
									<div class="imagem">
										<img src="assets/images/chamadas/{{ $chamada->imagem }}" alt="{{ $chamada->titulo }}">
									</div>
									<div class="caixa-de-texto">
										<h2>DESTAQUE CASUAL</h2>
										<p>
											{{ $chamada->texto }}
										</p>
									</div>
								</a>
							@endforeach
						@endif
						@if(sizeof($chamadas_blog))
							@foreach($chamadas_blog as $chamada)
								<a href="blog/post/{{ $chamada->slug }}" title="{{ $chamada->titulo }}" class="banner banner-blog">
									<div class="imagem">
										<img src="assets/images/blog/chamadas/{{ $chamada->capa }}" alt="{{ $chamada->titulo }}">
									</div>
									<div class="caixa-de-texto">
										<h2>DESTAQUE BLOG</h2>
										<p>
											{{ $chamada->titulo }}
										</p>
									</div>
								</a>
							@endforeach
						@endif
					</div>
					<div class="pager"></div>
				</div>
				<div class="esquerda-mobile">
					@for ($i = 0; $i <= 5; $i++)
						@if(isset($produtos[$i]))
							@if($produtos[$i]->link != '')
								<a href="{{ $produtos[$i]->link }}" class="hoverable thumb-produto">
									<img src="assets/images/siteimagemhome/{{ $produtos[$i]->imagem }}">
								</a>
							@else
								<div class="thumb-produto">
									<img src="assets/images/siteimagemhome/{{ $produtos[$i]->imagem }}">
								</div>
							@endif
						@endif
					@endfor
				</div>
			</div>
			<div class="inferior">
				@for ($i = 6; $i <= 10; $i++)
					@if(isset($produtos[$i]))
						@if($produtos[$i]->link != '')
							<a href="{{ $produtos[$i]->link }}" class="hoverable thumb-produto">
								<img src="assets/images/siteimagemhome/{{ $produtos[$i]->imagem }}">
							</a>
						@else
							<div class="thumb-produto">
								<img src="assets/images/siteimagemhome/{{ $produtos[$i]->imagem }}">
							</div>
						@endif
					@endif
				@endfor
			</div>
		</div>

	</div>

@stop