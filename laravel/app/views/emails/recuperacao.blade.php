<!DOCTYPE html>
<html>
<head>
    <title>Recuperação de Senha</title>
    <meta charset="utf-8">
</head>
<body>
    <h1>Recuperação de Senha</h1>
    <p>Olá, {{ $usuario->nome }}.</p>
    <p>Sua nova senha para acessar o Catálogo Casual é <strong>{{ $nova_senha }}</strong></p>
    <p><a href="http://www.casualmoveis.com.br/catalogo" target="blank">Clique aqui para acessar o Catálogo e lembre-se de alterar a senha para a de sua preferência após o 1&ordm; acesso.</a></p>
    <hr>
    <p>
        Att,<br>
        Casual Móveis
    </p>
</body>
</html>
