@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Blog - Posts <a href='{{ URL::route('painel.blogposts.create', array('blog_categorias_id' => $filtro)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Post</a>
    </h2>

    <hr>

    <div class="btn-group">
        <a href="{{ URL::route('painel.blogposts.index') }}" title="todos" class="btn btn-sm btn-default @if($filtro == '') btn-warning @endif">todas as categorias</a>
        @if(sizeof($listaCategorias))
            @foreach ($listaCategorias as $cat)
                <a href="{{ URL::route('painel.blogposts.index', array('blog_categorias_id' => $cat->id)) }}" class="btn btn-sm btn-default @if($filtro == $cat->id) btn-warning @endif">{{$cat->titulo}}</a>
            @endforeach
        @endif
    </div>

    <hr>

    <div class="input-group" style="width:300px;">
        <input type="text" class="form-control" id="filtrar-tabela" placeholder="Buscar Post">
        <span class="input-group-btn">
            <button class="btn btn-default" id="filtrar-tabela-btn" type="button"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
				<th>Publicado</th>
                <th>Título</th>
                <th>Data</th>
				<th>Categoria</th>
                <th><span class="glyphicon glyphicon-list"></span></th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
            @if(sizeof($registros))
                @foreach ($registros as $registro)

                    <tr class="tr-row" id="row_{{ $registro->id }}">
        				<td style="width:80px; text-align:center;">
                            @if($registro->publicar == 1)
                                <span class="text-success glyphicon glyphicon-ok"></span>
                            @else
                                <span class="text-danger glyphicon glyphicon-remove"></span>
                            @endif
                        </td>
                        <td>{{ $registro->titulo }}</td>
                        <td>{{ Tools::converteData($registro->data) }}</td>
        				<td>{{ $registro->categoria->titulo or "Não encontrada" }}</td>
                        <td><a href="{{URL::route('painel.blogcomentarios.index', array('blog_posts_id' => $registro->id))}}" title="Comentários" class="btn btn-sm btn-default">comentários</a></td>
                        <td class="crud-actions">
                            <a href='{{ URL::route('painel.blogposts.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
        					{{ Form::open(array('route' => array('painel.blogposts.destroy', $registro->id), 'method' => 'delete')) }}
        						<button type='submit' class='btn btn-danger btn-sm btn-delete' {{ Tools::exclusaoElementosFilhos('blog_posts', $registro) }}>excluir</button>
        					{{ Form::close() }}
                        </td>
                    </tr>

                @endforeach
            @else
                <tr>
                    <td colspan="6">
                        <h4 style="text-align:center;">Nenhum Post Encontrado</h4>
                    </td>
                </tr>
            @endif
        </tbody>

    </table>

    {{ $registros->appends(array('blog_categorias_id' => $filtro))->links() }}
</div>

@stop