@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Post
        </h2>

		<form action="{{URL::route('painel.blogposts.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label for="inputCategoria">Categoria</label>
					<select	name="blog_categorias_id" class="form-control" id="inputCategoria" required>
						<option value="">Selecione</option>
						@if(sizeof($listaCategorias))
							@foreach($listaCategorias as $cat)
								<option value="{{$cat->id}}" @if($filtro == $cat->id) selected @endif>{{$cat->titulo}}</option>
							@endforeach
						@endif
					</select>
				</div>

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputData">Data</label>
					<input type="text" class="form-control datepicker" id="inputData" name="data" @if(Session::has('formulario')) value="{{ Session::get('formulario.data') }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputImagem">Imagem de Capa</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<div class="form-group">
					<label for="inputTags">Tags</label>
					<div class="alert alert-block alert-warning">As tags podem ser inseridas com o <strong>ENTER</strong> ou separadas com <strong>vírgula</strong></div>
					<input type="text" class="form-control" id="inputTags" name="tags" @if(Session::has('formulario')) value="{{ Session::get('formulario.tags') }}" @endif>
				</div>

				<input type="hidden" id="tagsDisponiveis" value="@if(sizeof($listaTags))@foreach($listaTags as $t){{$t->titulo.','}}@endforeach@endif">

			</div>

			<div class="form-group">
				<label for="inputTexto">Texto</label>
				<textarea name="texto" class="cke form-control" id="inputTexto" >@if(Session::has('formulario')) {{ Session::get('formulario.texto') }} @endif</textarea>
			</div>

			<div class="pad">
				<div class="well">
					<div class="form-group">
						<label><input type="checkbox" value="1" name="publicar"> Publicar</label>
					</div>
				</div>
			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.blogposts.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop