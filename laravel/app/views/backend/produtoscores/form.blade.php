@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Cor
        </h2>  

		<form action="{{URL::route('painel.produtoscores.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>

				<div class="well">
					<label>Selecione uma Representação da Cor</label>
					<hr>
					<input type="hidden" name="representacao" value="hexa" id="cores-representacao">
					
					<ul class="nav nav-tabs" id="tabs-representacao-cor">
						<li class="active">
							<a href="#representacao-hexa" data-toggle="tab" data-ativar="hexa">
								Código Hexadecimal
							</a>
						</li>
						<li>
							<a href="#representacao-imagem" data-toggle="tab" data-ativar="imagem">
								Imagem
							</a>
						</li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane active fade in" id="representacao-hexa">
							<div class="form-group">
								<input type="text" class="form-control" id="inputHEX" placeholder="#FFFFFF" name="hexa" @if(Session::has('formulario')) value="{{ Session::get('formulario.hexa') }}" @endif >
							</div>
						</div>
						<div class="tab-pane fade" id="representacao-imagem">
							<div class="form-group">
								<label for="inputImagem">Imagem</label>
								<input type="file" class="form-control" id="inputImagem" name="imagem" >
							</div>
						</div>
					</div>
				</div>
				
			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.produtoscores.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop