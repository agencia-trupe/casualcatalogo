@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Programa de Relacionamento - Ativar Participante
    </h2>

    <p>
        Ativar Cadastros do Catálogo que não estão participando do Programa de Relacionamento
    </p>

    <hr>

    <div class="input-group" style="width:300px;">
        <input type="text" class="form-control" id="filtrar-tabela" placeholder="Buscar">
        <span class="input-group-btn">
            <button class="btn btn-default" id="filtrar-tabela-btn" type="button"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div>

    <a href="{{URL::route('painel.programaparticipantes.index')}}" style='margin-top:15px;' title="Voltar" class="btn btn-default btn-voltar">&larr; Voltar</a>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Nome</th>
				<th>E-mail</th>
				<th>CPF</th>
				<th>Empresa</th>
                <th>Cidade</th>
				<th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
				<td>{{ $registro->email }}</td>
				<td>{{ $registro->cpf }}</td>
				<td>{{ $registro->empresa }}</td>
                <td>{{ $registro->cidade.' - '.$registro->estado }}</td>
				<td class="crud-actions" style='white-space:nowrap;'>
                    {{ Form::open(array('route' => array('painel.programaparticipantes.ativar'))) }}
                        <div class="form-inline">
                            <div class="form-group">
                                <select name="tipo_participacao_relacionamento" required class='form-control'>
                                    <option value="">Selecione o Tipo de Perfil</option>
                                    <option value="arquiteto">Arquiteto</option>
                                    <option value="assistente">Assistente</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="programa_lojas_id" required class='form-control'>
                                    @if(Usuarios::isAssistPrograma())

                                        @if(sizeof($listaLojas))
                                            @foreach ($listaLojas as $loja)
                                                @if($loja->id == Auth::painel()->user()->loja->id)
                                                    <option value="{{ $loja->id }}" selected @if(Session::has('formulario') && Session::get('formulario.programa_lojas_id') == $loja->id) selected @endif>{{ $loja->titulo }}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            <option value="">Nenhuma Loja Cadastrada</option>
                                        @endif

                                    @else

                                        @if(sizeof($listaLojas))
                                            <option value="">Selecione a Loja</option>
                                            @foreach ($listaLojas as $loja)
                                                <option value="{{ $loja->id }}">{{ $loja->titulo }}</option>
                                            @endforeach
                                        @else
                                            <option value="">Nenhuma Loja Cadastrada</option>
                                        @endif

                                    @endif
                                </select>
                            </div>
                            <input type="hidden" name="id" value="{{ $registro->id }}">
    						<button type='submit' class='btn btn-primary btn-sm'>Ativar &rarr;</button>
                        </div>
					{{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

</div>

@stop
