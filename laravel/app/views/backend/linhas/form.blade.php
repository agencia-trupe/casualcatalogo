@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Linha
        </h2>

        <h3>
	        Fornecedor : <span class="text-info">{{ $fornecedor->titulo . ' ('.$fornecedor->divisao.')' }}</span>
	    </h3>

		<form action="{{URL::route('painel.linhas.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título da Linha</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>

				<input type="hidden" name="fornecedores_id" value="{{ $fornecedor->id }}">

			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.linhas.index', array('fornecedores_id' => $fornecedor->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop