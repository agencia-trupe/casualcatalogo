@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Linha
        </h2>

        <h3>
	        Fornecedor : <span class="text-info">{{ $fornecedor->titulo . ' ('.$fornecedor->divisao.')' }}</span>
	    </h3>

		{{ Form::open( array('route' => array('painel.linhas.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título da Linha</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<input type="hidden" name="fornecedores_id" value="{{ $fornecedor->id }}">

			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.linhas.index', array('fornecedores_id' => $fornecedor->id))}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop