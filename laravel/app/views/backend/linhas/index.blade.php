@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Linhas <a href='{{ URL::route('painel.linhas.create', array('fornecedores_id' => $fornecedor->id)) }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Linha</a>
    </h2>

    <h3>
        Fornecedor : <span class="text-info">{{ $fornecedor->titulo . ' ('.$fornecedor->divisao.')' }}</span>
    </h3>

    <hr>

    <a href="{{ URL::route('painel.fornecedores.index') }}" title="Voltar" class="btn btn-default">&larr; voltar para Fornecedores</a>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='linhas'>

        <thead>
            <tr>
                <th>Ordenar</th>
				<th>Título</th>
				<th>Fornecedor</th>
                <th><span class="glyphicon glyphicon-list"></span></th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @if(sizeof($fornecedor->linhas))
            @foreach ($fornecedor->Linhas as $registro)

                <tr class="tr-row" id="row_{{ $registro->id }}">
                    <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
    				<td>{{ $registro->titulo }}</td>
    				<td>{{ $registro->fornecedor()->titulo or "Todos" }}</td>
                    <td>
                        <a href="{{ URL::route('painel.produtos.index', array('linhas_id' => $registro->id)) }}" title="Produtos da Linha" class="btn btn-sm btn-default">Produtos</a>
                    </td>
                    <td class="crud-actions">
                        <a href='{{ URL::route('painel.linhas.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
    					{{ Form::open(array('route' => array('painel.linhas.destroy', $registro->id), 'method' => 'delete')) }}
    						<button type='submit' class='btn btn-danger btn-sm btn-delete' {{ Tools::exclusaoElementosFilhos('linhas', $registro) }}>excluir</button>
    					{{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        @else
            <tr>
                <td colspan="5" style="text-align:center;"><h4>Nenhuma Linha cadastrada para o Fornecedor</h4></td>
            </tr>
        @endif
        </tbody>

    </table>


</div>

@stop