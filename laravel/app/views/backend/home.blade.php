@section('conteudo')

	<div class="container">

		@if(Session::has('mensagem'))
			<div class="alert alert-warning">{{ Session::get('mensagem') }}</div>
		@endif

		<h1>Bem Vindo(a),</h1>

		<br>
		<?php setlocale(LC_TIME, 'pt_BR'); ?>
		<p>{{ \Carbon\Carbon::now('America/Sao_Paulo')->formatLocalized('%d de %B de %Y, %A') }}.</p>

		<p>Pelo painel administrativo da trupe você pode controlar as principais funções e conteúdo do site.</p>

		<p>Qualquer Dúvida entrar em contato: <a href="mailto:contato@trupe.net">contato@trupe.net</a></p>

	</div>

@stop
