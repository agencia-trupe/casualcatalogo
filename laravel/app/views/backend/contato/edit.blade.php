@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Contato
        </h2>  

		{{ Form::open( array('route' => array('painel.contato.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" class="form-control" id="inputFacebook" name="facebook" value="{{$registro->facebook}}" >
				</div>
				
				<div class="form-group">
					<label for="inputTwitter">Twitter</label>
					<input type="text" class="form-control" id="inputTwitter" name="twitter" value="{{$registro->twitter}}" >
				</div>
				
				<div class="form-group">
					<label for="inputFlickr">Flickr</label>
					<input type="text" class="form-control" id="inputFlickr" name="flickr" value="{{$registro->flickr}}" >
				</div>
				
				<div class="form-group">
					<label for="inputInstagram">Instagram</label>
					<input type="text" class="form-control" id="inputInstagram" name="instagram" value="{{$registro->instagram}}" >
				</div>
				
				<div class="well">
					{{ Tools::embed($registro->maps, '100%', '400px') }}
					<hr>
					<div class="form-group">
						<label for="inputMaps">Código de Incorporação do Google Maps</label>
						<input type="text" class="form-control" id="inputMaps" name="maps" value='{{$registro->maps}}'>
					</div>
				</div>
				
			</div>

			<hr>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop