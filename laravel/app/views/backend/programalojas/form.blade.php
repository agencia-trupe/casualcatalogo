@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Loja
        </h2>  

		<form action="{{URL::route('painel.programalojas.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputNome da Loja">Nome da Loja</label>
					<input type="text" class="form-control" id="inputNome da Loja" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputE-mail de Contato">E-mail de Contato</label>
					<input type="text" class="form-control" id="inputE-mail de Contato" name="email" @if(Session::has('formulario')) value="{{ Session::get('formulario.email') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<textarea name="endereco" class="form-control" id="inputEndereço" >@if(Session::has('formulario')) {{ Session::get('formulario.endereco') }} @endif</textarea>
				</div>
				
			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.programalojas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop