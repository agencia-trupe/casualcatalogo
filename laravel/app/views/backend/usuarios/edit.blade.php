@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Usuário do Painel Administrativo
        </h2>

		{{ Form::open( array('route' => array('painel.usuarios.update', $usuario->id), 'files' => false, 'method' => 'put') ) }}
			<div class="pad">

        	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label for="inputUsuario">Usuário</label>
					<input type="text" class="form-control" id="inputUsuario" name="username"  value="{{ $usuario->username }}" required>
				</div>

				<div class="form-group">
					<label for="inputEmail">E-mail</label>
					<input type="email" class="form-control" id="inputEmail" name="email" value="{{ $usuario->email }}">
				</div>

				<div class="form-group">
					<label for="inputSenha">Senha</label>
					<input type="password" class="form-control" id="inputSenha" name="password">
				</div>

				<div class="form-group">
					<label for="inputConfSenha">Digite novamente a Senha</label>
					<input type="password" class="form-control" id="inputConfSenha" name="password_confirm">
				</div>

				<div class="well">
					<strong>Tipo de usuário</strong>
					<hr>
					<ul>
						<li><label><input type="radio" name="tipo_usuario" value="admin" required @if($usuario->admin_geral == 1) checked @endif > Admin Geral</label></li>
						<li><label><input type="radio" name="tipo_usuario" value="assist_produtos" @if($usuario->admin_geral == 0 && $usuario->assist_produtos == 1) checked @endif > Assistente Produtos</label></li>
						<li><label><input type="radio" name="tipo_usuario" value="assist_programa" @if($usuario->admin_geral == 0 && $usuario->assist_programa == 1) checked @endif > Assistente Programa de Relacionamentos</label></li>
            <li><label><input type="radio" name="tipo_usuario" value="gerente" @if($usuario->admin_geral == 0 && $usuario->is_gerente == 1) checked @endif > Gerente</label></li>
					</ul>

					<div class="form-group listaLojas escondido">

						<hr>

						<label for="selectLoja">O Assistente Programa de Relacionamentos precisa ser associado à uma Loja</label>

						<select name="programa_lojas_id" id="selectLoja" class="form-control">
							@if(sizeof($listaLojas))
								<option value=""></option>
								@foreach ($listaLojas as $loja)
									<option value="{{ $loja->id }}" @if($loja->id == $usuario->programa_lojas_id) selected @endif>{{ $loja->titulo }}</option>
								@endforeach
							@else
								<option value="">Nenhuma Loja Cadastrada</option>
							@endif
						</select>

					</div>
				</div>
			</div>
			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop
