@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Usuário do Painel Administrativo
        </h2>

		<form action="{{URL::route('painel.usuarios.store')}}" method="post">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label for="inputUsuario">Usuário</label>
					<input type="text" class="form-control" id="inputUsuario" name="username"  @if(Session::has('formulario')) value="{{ Session::get('formulario.username') }}" @endifrequired>
				</div>

				<div class="form-group">
					<label for="inputEmail">E-mail</label>
					<input type="email" class="form-control" id="inputEmail" name="email" @if(Session::has('formulario')) value="{{ Session::get('formulario.email') }}" @endif>
				</div>

				<div class="form-group">
					<label for="inputSenha">Senha</label>
					<input type="password" class="form-control" id="inputSenha" name="password" required>
				</div>

				<div class="form-group">
					<label for="inputConfSenha">Digite novamente a Senha</label>
					<input type="password" class="form-control" id="inputConfSenha" name="password_confirm" required>
				</div>

				<div class="well">
					<strong>Tipo de usuário</strong>
					<hr>
					<ul>
						<li><label><input type="radio" name="tipo_usuario" value="admin" required> Admin Geral</label></li>
						<li><label><input type="radio" name="tipo_usuario" value="assist_produtos"> Assistente Produtos</label></li>
						<li><label><input type="radio" name="tipo_usuario" value="assist_programa"> Assistente Programa de Relacionamentos</label></li>
            <li><label><input type="radio" name="tipo_usuario" value="gerente"> Gerente</label></li>
					</ul>

					<div class="form-group listaLojas escondido">

						<label for="selectLoja">O Assistente Programa de Relacionamentos precisa ser associado à uma Loja</label>

						<select name="programa_lojas_id" id="selectLoja" class="form-control">
							@if(sizeof($listaLojas))
								<option value=""></option>
								@foreach ($listaLojas as $loja)
									<option value="{{ $loja->id }}">{{ $loja->titulo }}</option>
								@endforeach
							@else
								<option value="">Nenhuma Loja Cadastrada</option>
							@endif
						</select>

					</div>
				</div>
			</div>

			<hr>

			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop
