@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Blog - Comentários do Post : <span class="text-info">{{$post->titulo}}</span>
    </h2>

    <hr>

    <a href="{{URL::route('painel.blogposts.index')}}" class="btn btn-sm btn-default">&larr; voltar para Posts</a>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
				<th>Aprovado</th>
                <th>Autor</th>
                <th>Email</th>
                <th>Data</th>
                <th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @if(sizeof($comentarios))
            @foreach ($comentarios as $registro)

                <tr class="tr-row" id="row_{{ $registro->id }}">
                    <td style="width:80px; text-align:center;">
                        @if($registro->aprovado == 1)
                            <span class="text-success glyphicon glyphicon-ok"></span>
                        @else
                            <span class="text-danger glyphicon glyphicon-remove"></span>
                        @endif
                    </td>
                    <td>{{ $registro->autor }}</td>
    				<td>{{ $registro->email }}</td>
    				<td>{{ Tools::converteData($registro->data) }}</td>
    				<td>{{ Str::words(strip_tags($registro->texto), 15) }}</td>
    				<td class="crud-actions" style="width:150px">
                        <a href='{{ URL::route('painel.blogcomentarios.edit', $registro->id ) }}' class='btn btn-default btn-sm'>visualizar</a>
    					{{ Form::open(array('route' => array('painel.blogcomentarios.destroy', $registro->id), 'method' => 'delete')) }}
    						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
    					{{ Form::close() }}
                    </td>
                </tr>

            @endforeach
        @else
            <tr>
                <td colspan="6" style="text-align:center"><h4>Nenhum Comentário para o Post</h4></td>
            </tr>
        @endif
        </tbody>

    </table>

    {{ $comentarios->appends(array('blog_posts_id' => $post->id))->links() }}
</div>

@stop