@section('conteudo')

    <div class="container add">

      	<h2>
        	Programa de Relacionamento - Editar Aditivo
        </h2>

		{{ Form::open( array('route' => array('painel.programaaditivos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<input type="hidden" name="programa_campanha_id" value="{{ $registro->programa_campanha_id }}">

		    	<div class="form-group">
					<label for="inputprograma_lojas_id">Aditivo para a Loja</label>
					<input type="text" disabled class="form-control" id="inputprograma_lojas_id" name="programa_lojas_id_disabled" value="{{$registro->loja->titulo or 'Não encontrado'}}">
					<input type="hidden" name="programa_lojas_id" value="{{ $registro->programa_lojas_id }}">
				</div>

			</div>

			<div class="form-group">
				<label for="inputtexto_arquiteto">Texto para Arquitetos</label>
				<textarea name="texto_arquiteto" class="cke form-control" id="inputtexto_arquiteto" >{{$registro->texto_arquiteto }}</textarea>
			</div>

			<div class="form-group">
				<label for="inputtexto_assistente">Texto para Assistentes</label>
				<textarea name="texto_assistente" class="cke form-control" id="inputtexto_assistente" >{{$registro->texto_assistente }}</textarea>
			</div>

			<hr>
			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{URL::route('painel.programaaditivos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop