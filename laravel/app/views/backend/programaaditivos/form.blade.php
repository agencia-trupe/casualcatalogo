@section('conteudo')

    <div class="container add">

      	<h2>
        	Programa de Relacionamento - Adicionar Aditivo
        </h2>

		<form action="{{URL::route('painel.programaaditivos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<input type="hidden" name="programa_campanha_id" value="{{ $campanha->id }}">

				<div class="form-group">
					<label for="programaLojasID">Aditivo para a Loja</label>
					<select name="programa_lojas_id" id="programaLojasID" class="form-control" required>
						@foreach($listaLojas as $loja)
							@if(!in_array($loja->id, $lojas_com_aditivo))
								<option value="{{ $loja->id }}">{{ $loja->titulo }}</option>
							@endif
						@endforeach
					</select>
				</div>

			</div>

			<div class="form-group">
				<label for="inputtexto_arquiteto">Texto para Arquitetos</label>
				<textarea name="texto_arquiteto" class="cke form-control" id="inputtexto_arquiteto" >@if(Session::has('formulario')) {{ Session::get('formulario.texto_arquiteto') }} @endif</textarea>
			</div>

			<div class="form-group">
				<label for="inputtexto_assistente">Texto para Assistentes</label>
				<textarea name="texto_assistente" class="cke form-control" id="inputtexto_assistente" >@if(Session::has('formulario')) {{ Session::get('formulario.texto_assistente') }} @endif</textarea>
			</div>

			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.programaaditivos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>

@stop