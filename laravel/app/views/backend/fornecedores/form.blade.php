@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Fornecedor
        </h2>  

		<form action="{{URL::route('painel.fornecedores.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="well">
					<div class="form-group">
						<strong>Divisão</strong>
						<hr>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="radio" name="divisao" value="interiores" required> Interiores</label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="radio" name="divisao" value="exteriores" required> Exteriores</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ Session::get('formulario.titulo') }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>
				
				<div class="form-group">
					<label for="inputObservações">Observações</label>
					<textarea name="obs" class="form-control" id="inputObservações" >@if(Session::has('formulario')) {{ Session::get('formulario.obs') }} @endif</textarea>
				</div>

				<div class="well">
					<div class="form-group">
						<strong>Publicação</strong>
						<hr>
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="checkbox" name="publicar_site" value="1" @if(Session::has('formulario') && Session::get('formulario.publicar_site') == 1) checked @endif> Publicar no Site</label>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
									<label><input type="checkbox" name="publicar_catalogo" value="1" @if(Session::has('formulario') && Session::get('formulario.publicar_catalogo') == 1) checked @endif> Publicar no Catálogo</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<hr>

				<div class="well">
				<label>Arquivos</label>

				<div id="multiUpload">
					<div id="icone">
						<span class="glyphicon glyphicon-open"></span>
						<span class="glyphicon glyphicon-refresh"></span>
					</div>
					<p>
						Envie os arquivos em PDF do Fornecedor. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
						Se preferir também pode utilizar o botão abaixo para selecioná-los.
					</p>
					<input id="fileupload-arquivos" type="file" name="files[]" data-url="painel/multi-upload/arquivos/fornecedores" multiple>
				</div>
				<div id="listaArquivos"><ul></ul></div>
				<hr>
				<div class="panel panel-default">
					<div class="panel-body">
				    	Você pode clicar e arrastar os arquivos para ordená-los.
				  	</div>				  	
				</div>
			</div>
				
			</div>
			<hr>
			<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

			<a href="{{URL::route('painel.fornecedores.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>
    </div>
    
@stop