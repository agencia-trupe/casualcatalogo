@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Pedidos de Orçamento
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Usuário</th>
				<th>Loja</th>
                <th>Data</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->solicitante->nome or 'Não encontrado' }}</td>
				<td>{{ $registro->loja->titulo or 'Não encontrado' }}</td>
                <td>{{ \Carbon\Carbon::parse($registro->created_at)->formatLocalized('%d/%m/%Y') }}</td>
                <td class="crud-actions" style="width:155px;">
                    <a href='{{ URL::route('painel.pedidos.show', $registro->id ) }}' class='btn btn-primary btn-sm'>visualizar</a>
					{{ Form::open(array('route' => array('painel.pedidos.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop