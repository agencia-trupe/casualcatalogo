<?php

Validator::extend('intervalo_data_ja_existe', function($attribute, $value, $parameters)
{
	$check = ProgramaCampanhas::where('data_inicio', '<=', Tools::converteData(Input::get($attribute)))
							  ->where('data_termino', '>=', Tools::converteData(Input::get($attribute)));

	if(isset($parameters[0]))
		$check->where('id', '!=', $parameters[0]);

	return sizeof($check->get()) > 0 ? false : true;
});

Validator::extend('contem_intervalo_data', function($attribute, $value, $parameters)
{

	$data_inicio = $attribute == 'data_inicio' ? Input::get('data_inicio') : $parameters[0];
	$data_termino = $attribute == 'data_termino' ? Input::get('data_termino') : $parameters[0];

	$check = ProgramaCampanhas::where('data_inicio', '>=', Tools::converteData($data_inicio))
							  ->where('data_termino', '<=', Tools::converteData($data_termino));

	if(isset($parameters[1]))
		$check->where('id', '!=', $parameters[1]);

	return sizeof($check->get()) > 0 ? false : true;
});

Validator::extend('custom_data', function($attribute, $value, $parameters)
{
	return preg_match('/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/', Input::get($attribute));
});

Validator::extend('antes_de', function($attribute, $value, $parameters)
{
	return strtotime(str_replace('/', '-', Input::get($attribute))) < strtotime(str_replace('/', '-', $parameters[0]));
});

Validator::extend('depois_de', function($attribute, $value, $parameters)
{
	return strtotime(str_replace('/', '-', Input::get($attribute))) > strtotime(str_replace('/', '-', $parameters[0]));
});

Validator::extend('menor_que', function($attribute, $value, $parameters)
{
	$val1 = str_replace('.', '', Input::get($attribute));
	$val1 = str_replace(',', '.', $val1);

	$val2 = str_replace('.', '', $parameters[0]);
	$val2 = str_replace(',', '.', $val2);

	return (float) $val1 < (float) $val2;
});

Validator::extend('maior_que', function($attribute, $value, $parameters)
{
	$val1 = str_replace('.', '', Input::get($attribute));
	$val1 = str_replace(',', '.', $val1);

	$val2 = str_replace('.', '', $parameters[0]);
	$val2 = str_replace(',', '.', $val2);

	return (float) $val1 > (float) $val2;
});

Validator::extend('intervalo_pontos_ja_existe_premios', function($attribute, $value, $parameters)
{
	// $parametes[0] = ID CAMPANHA
	// $parametes[1] = TIPO DO PREMIO
	// $parametes[2] = ID a IGNORAR
	$pontos = Input::get($attribute);
	$pontos = str_replace('.', '', $pontos);
	$pontos = str_replace(',', '.', $pontos);

	$check = ProgramaPremios::where('inicio_pontuacao', '<=', (float) $pontos * 100)
							->where('fim_pontuacao', '>=', (float) $pontos * 100)
							->where('tipo', '=', $parameters[1])
							->where('programa_campanha_id', '=', $parameters[0]);

	if(isset($parameters[2]))
		$check->where('id', '!=', $parameters[2]);

	return sizeof($check->get()) > 0 ? false : true;
});

Validator::extend('contem_intervalo_pontos_premios', function($attribute, $value, $parameters)
{
	// $parametes[0] = ID CAMPANHA
	// $parametes[1] = DATA PARA COMPARAR
	// $parametes[2] = TIPO DO PREMIO
	// $parametes[3] = ID a IGNORAR
	$pontos_inicio = $attribute == 'inicio_pontuacao' ? Input::get('inicio_pontuacao') : $parameters[1];
	$pontos_termino = $attribute == 'fim_pontuacao' ? Input::get('fim_pontuacao') : $parameters[1];

	$pontos_inicio = str_replace('.', '', $pontos_inicio);
	$pontos_inicio = str_replace(',', '.', $pontos_inicio);

	$pontos_termino = str_replace('.', '', $pontos_termino);
	$pontos_termino = str_replace(',', '.', $pontos_termino);

	$check = ProgramaPremios::where('inicio_pontuacao', '>=', (float) $pontos_inicio * 100)
							  ->where('fim_pontuacao', '<=', (float) $pontos_termino * 100)
							  ->where('tipo', '=', $parameters[2])
							  ->where('programa_campanha_id', '=', $parameters[0]);

	if(isset($parameters[3]))
		$check->where('id', '!=', $parameters[3]);

	return sizeof($check->get()) > 0 ? false : true;
});

Validator::extend('intervalo_pontos_ja_existe_emails', function($attribute, $value, $parameters)
{
	// $parametes[0] = ID CAMPANHA
	// $parametes[1] = TIPO DO EMAIL
	// $parametes[2] = ID a IGNORAR
	$pontos = Input::get($attribute);
	$pontos = str_replace('.', '', $pontos);
	$pontos = str_replace(',', '.', $pontos);

	$check = ProgramaEmails::where('inicio_pontuacao', '<=', (float) $pontos * 100)
							->where('fim_pontuacao', '>=', (float) $pontos * 100)
							->where('tipo', '=', $parameters[1])
							->where('programa_campanha_id', '=', $parameters[0]);

	if(isset($parameters[2]))
		$check->where('id', '!=', $parameters[2]);

	return sizeof($check->get()) > 0 ? false : true;
});

Validator::extend('contem_intervalo_pontos_emails', function($attribute, $value, $parameters)
{
	// $parametes[0] = ID CAMPANHA
	// $parametes[1] = DATA PARA COMPARAR
	// $parametes[2] = TIPO DO EMAIL
	// $parametes[3] = ID a IGNORAR
	$pontos_inicio = $attribute == 'inicio_pontuacao' ? Input::get('inicio_pontuacao') : $parameters[1];
	$pontos_termino = $attribute == 'fim_pontuacao' ? Input::get('fim_pontuacao') : $parameters[1];

	$pontos_inicio = str_replace('.', '', $pontos_inicio);
	$pontos_inicio = str_replace(',', '.', $pontos_inicio);

	$pontos_termino = str_replace('.', '', $pontos_termino);
	$pontos_termino = str_replace(',', '.', $pontos_termino);

	$check = ProgramaEmails::where('inicio_pontuacao', '>=', (float) $pontos_inicio * 100)
							  ->where('fim_pontuacao', '<=', (float) $pontos_termino * 100)
							  ->where('tipo', '=', $parameters[2])
							  ->where('programa_campanha_id', '=', $parameters[0]);

	if(isset($parameters[3]))
		$check->where('id', '!=', $parameters[3]);

	return sizeof($check->get()) > 0 ? false : true;
});

Validator::extend('intervalo_pontos_ja_existe_chamadas', function($attribute, $value, $parameters)
{
	// $parametes[0] = ID CAMPANHA
	// $parametes[1] = TIPO DA CHAMADA
	// $parametes[2] = ID a IGNORAR
	$pontos = Input::get($attribute);
	$pontos = str_replace('.', '', $pontos);
	$pontos = str_replace(',', '.', $pontos);

	$check = ProgramaChamadas::where('inicio_pontuacao', '<=', (float) $pontos * 100)
							->where('fim_pontuacao', '>=', (float) $pontos * 100)
							->where('tipo', '=', $parameters[1])
							->where('programa_campanha_id', '=', $parameters[0]);

	if(isset($parameters[2]))
		$check->where('id', '!=', $parameters[2]);

	return sizeof($check->get()) > 0 ? false : true;
});

Validator::extend('contem_intervalo_pontos_chamadas', function($attribute, $value, $parameters)
{
	// $parametes[0] = ID CAMPANHA
	// $parametes[1] = DATA PARA COMPARAR
	// $parametes[2] = TIPO DA CHAMADA
	// $parametes[3] = ID a IGNORAR
	$pontos_inicio = $attribute == 'inicio_pontuacao' ? Input::get('inicio_pontuacao') : $parameters[1];
	$pontos_termino = $attribute == 'fim_pontuacao' ? Input::get('fim_pontuacao') : $parameters[1];

	$check = ProgramaChamadas::where('inicio_pontuacao', '>=', $pontos_inicio)
							  ->where('fim_pontuacao', '<=', $pontos_termino)
							  ->where('tipo', '=', $parameters[2])
							  ->where('programa_campanha_id', '=', $parameters[0]);

	if(isset($parameters[3]))
		$check->where('id', '!=', $parameters[3]);

	return sizeof($check->get()) > 0 ? false : true;
});