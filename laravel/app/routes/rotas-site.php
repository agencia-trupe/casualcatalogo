<?php

/*******************************************/
/************** ROTAS DO SITE **************/
/*******************************************/

Route::group(array('namespace' => 'Site'), function()
{

	Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
	Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
	Route::get('painel', array('before' => 'auth.painel', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
	Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

	Route::post('blog/comentar', array('as' => 'blog.comentar', 'uses' => 'BlogController@comentar'));
	Route::any('blog/post/{slug_post?}', array('as' => 'blog.detalhes', 'uses' => 'BlogController@detalhes'));
	Route::any('blog/tag/{slug_tag?}', array('as' => 'blog', 'uses' => 'BlogController@porTags'));
	Route::any('blog/ano/{anoSelecionado?}/{mesSelecionado?}', array('as' => 'blog', 'uses' => 'BlogController@porAno'));
	Route::any('blog/{slug_categoria?}', array('as' => 'blog', 'uses' => 'BlogController@index'));

	Route::get('produtos', array('as' => 'produtos', 'uses' => 'ProdutosController@index'));
	Route::get('produtos/marcas', array('as' => 'produtos.marcas', 'uses' => 'ProdutosController@marcas'));
	Route::post('produtos/buscar', array('as' => 'produtos.buscar', 'uses' => 'ProdutosController@busca'));

	Route::get('designers', array('as' => 'designers', 'uses' => 'DesignersController@index'));
	Route::get('designers/{slug}', array('as' => 'designers.detalhes', 'uses' => 'DesignersController@detalhes'));

	Route::get('produtos/{divisao}/marcas/{slug_fornecedor?}/{slug_produto?}',
				array(
					'as' => 'produtos.lista',
					'uses' => 'ProdutosController@porMarca'
				))
				->where('divisao', 'interiores|exteriores');

	Route::get('produtos/{divisao}/{tipo_slug?}/{slug_produto?}',
				array(
					'as' => 'produtos.lista',
					'uses' => 'ProdutosController@lista'
				))
				->where(array('divisao' => 'interiores|exteriores'));


	Route::get('perfil', array('as' => 'perfil', 'uses' => 'PerfilController@index'));
	Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
	Route::post('contato', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));

	// Autenticação do Login
	Route::post('painel/login',  array('as' => 'painel.auth', function(){
		$authvars = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);

		$lembrar = (Input::get('lembrar') == '1') ? true : false;

		if(Auth::painel()->attempt($authvars, $lembrar)){
			return Redirect::to('painel');
		}else{
			Session::flash('login_errors', true);
			return Redirect::to('painel/login');
		}
	}));

	Route::get('painel/logout', array('as' => 'painel.off', function(){
		Auth::painel()->logout();
		return Redirect::to('painel');
	}));

	Route::post('ajax/gravaOrdem', array('before' => 'auth.painel', 'uses' => 'Painel\AjaxController@gravaOrdem'));

	Route::post('ajax/selecionarLinhas', array('as' => 'painel.selecionaLinhas', 'uses' => 'Painel\AjaxController@selecionaLinhas'));
	Route::post('ajax/selecionarProdutos', array('as' => 'painel.selecionaProdutos', 'uses' => 'Painel\AjaxController@selecionaProdutos'));
});
